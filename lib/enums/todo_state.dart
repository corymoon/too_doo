enum TodoState {
  active,
  complete,
  priority,
  snoozed,
  archived,
}
