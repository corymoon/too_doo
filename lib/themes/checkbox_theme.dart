import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/functions/functions.dart';

final CheckboxThemeData kAppCheckboxTheme = CheckboxThemeData(
  fillColor: materialStateColor(
    active: AppColors.primaryColor,
    inactive: AppColors.textColor50,
  ),
);
