import 'package:flutter/material.dart';

import '/constants/constants.dart';

AppBarTheme kAppAppBarTheme = AppBarTheme(
  backgroundColor: AppColors.backgroundColor,
  elevation: 0.0,
);
