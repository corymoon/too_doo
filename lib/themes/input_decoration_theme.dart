import 'package:flutter/material.dart';
import '/constants/constants.dart';

InputDecorationTheme kAppInputDecorationTheme = InputDecorationTheme(
  contentPadding: const EdgeInsets.symmetric(
    horizontal: 20.0,
    vertical: 15.0,
  ),
  filled: true,
  labelStyle: AppStyles.textFieldLabelStyle(AppColors.textFieldLabelColor),
  fillColor: AppColors.textFieldFillColor,
  alignLabelWithHint: true,
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(Numbers.borderRadiusLarge),
    borderSide: BorderSide.none,
  ),
);
