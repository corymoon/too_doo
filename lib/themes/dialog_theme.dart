import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/themes/themes.dart';

DialogTheme kAppDialogTheme = DialogTheme(
  backgroundColor: AppColors.elevatedSurfaceBackgroundColor,
  titleTextStyle: kAppTextTheme.headline6!.copyWith(color: AppColors.textColor),
  contentTextStyle: kAppTextTheme.subtitle1!.copyWith(
    color: AppColors.textColor,
  ),
  shape: AppShapes.allCornersLargeRadius,
);
