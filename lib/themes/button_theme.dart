import 'package:flutter/material.dart';

import '/constants/constants.dart';

ButtonThemeData kAppButtonTheme = ThemeData.light().buttonTheme.copyWith(
      shape: AppShapes.allCornersLargeRadius,
    );
