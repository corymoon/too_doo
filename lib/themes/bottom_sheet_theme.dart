import 'package:flutter/material.dart';
import '/constants/constants.dart';

BottomSheetThemeData kAppBottomSheetTheme = BottomSheetThemeData(
  backgroundColor: AppColors.elevatedSurfaceBackgroundColor,
  shape: AppShapes.bottomSheetShape,
);
