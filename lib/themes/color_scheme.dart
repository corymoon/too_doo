import 'package:flutter/material.dart';
import '/constants/constants.dart';

final ColorScheme kAppColorScheme = const ColorScheme.dark().copyWith(
  primary: AppColors.primaryColor,
  onPrimary: AppColors.textColor,
  surface: AppColors.primaryColor,
  error: AppColors.errorColor,
  onError: AppColors.textColor,
  brightness: Brightness.dark,
  secondary: AppColors.primaryColor,
  onSecondary: AppColors.textColor,
  secondaryVariant: AppColors.primaryColor,
  onSurface: AppColors.textColor,
  primaryVariant: AppColors.primaryColor,
  background: AppColors.backgroundColor,
  onBackground: AppColors.textColor,
);
