import 'package:flutter/material.dart';

import '/constants/constants.dart';

ElevatedButtonThemeData kAppElevatedButtonTheme = ElevatedButtonThemeData(
  style: ElevatedButton.styleFrom(
    elevation: 0.0,
    shape: AppShapes.allCornersLargeRadius,
  ),
);
