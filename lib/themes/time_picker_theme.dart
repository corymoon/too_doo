import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/functions/functions.dart';

final TimePickerThemeData kAppTimePickerTheme = TimePickerThemeData(
  backgroundColor: AppColors.elevatedSurfaceBackgroundColor,
  hourMinuteShape: AppShapes.allCornersMediumRadius,
  dayPeriodBorderSide: BorderSide.none,
  shape: AppShapes.allCornersLargeRadius,
  dayPeriodTextColor: AppColors.textColor,
  dayPeriodShape: AppShapes.allCornersSmallRadius,
  dayPeriodColor: materialStateColor(
    active: AppColors.primaryColor,
    inactive: AppColors.textColor12,
  ),
  hourMinuteColor: materialStateColor(
    active: AppColors.primaryColor,
    inactive: AppColors.textColor12,
  ),
  hourMinuteTextColor: materialStateColor(
    active: AppColors.textColor,
    inactive: AppColors.textColor70,
  ),
  dialHandColor: AppColors.primaryColor,
  dialBackgroundColor: AppColors.topSurfaceBackgroundColor,
  hourMinuteTextStyle: const TextStyle(
    fontSize: 35.0,
    fontWeight: FontWeight.bold,
  ),
  dayPeriodTextStyle: const TextStyle(
    fontSize: 15.0,
    fontWeight: FontWeight.bold,
  ),
  helpTextStyle: TextStyle(
    fontSize: 15.0,
    fontWeight: FontWeight.bold,
    color: AppColors.textColor70,
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: AppColors.textFieldFillColor,
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.circular(Numbers.borderRadiusMedium),
    ),
  ),
  dialTextColor: materialStateColor(
    active: AppColors.textColor,
    inactive: AppColors.textColor70,
  ),
  entryModeIconColor: AppColors.primaryColor,
);
