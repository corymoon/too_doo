import 'package:flutter/material.dart';

import '/constants/constants.dart';

NavigationBarThemeData kAppNavigationBarTheme = NavigationBarThemeData(
  backgroundColor: AppColors.elevatedSurfaceBackgroundColor,
  height: 70.0,
  indicatorColor: AppColors.topSurfaceBackgroundColor,
);
