import 'package:flutter/material.dart';

import '/constants/colors.dart';

class Space {
  static const _smallSpace = 4.0;
  static const _mediumSpace = 12.0;
  static const _largeSpace = 20.0;
  static const _xLargeSpace = 40.0;

  static const verticalSmall = SizedBox(height: _smallSpace);
  static const verticalMedium = SizedBox(height: _mediumSpace);
  static const verticalLarge = SizedBox(height: _largeSpace);
  static const verticalXLarge = SizedBox(height: _xLargeSpace);

  static const horizontalSmall = SizedBox(width: _smallSpace);
  static const horizontalMedium = SizedBox(width: _mediumSpace);
  static const horizontalLarge = SizedBox(width: _largeSpace);
  static const horizontalXLarge = SizedBox(width: _xLargeSpace);
}

class AppWidgets {
  static final divider = Divider(
    color: AppColors.textColor12,
    // indent: 20.0,
    // endIndent: 20.0,
  );
}
