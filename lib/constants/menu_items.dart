import '/constants/constants.dart';
import '/models/models.dart';

final List<AppMenuItem> kAppMenuItems = [
  AppMenuItem(
    index: 0,
    activeIcon: AppIcons.todosIcon.active,
    inactiveIcon: AppIcons.todosIcon.inactive,
    label: Strings.todosScreenTitle,
  ),
  AppMenuItem(
    index: 1,
    activeIcon: AppIcons.archiveIcon.active,
    inactiveIcon: AppIcons.archiveIcon.inactive,
    label: Strings.archiveScreenTitle,
  ),
  AppMenuItem(
    index: 2,
    activeIcon: AppIcons.feedIcon.active,
    inactiveIcon: AppIcons.feedIcon.inactive,
    label: Strings.feedScreenTitle,
  ),
  AppMenuItem(
    index: 3,
    activeIcon: AppIcons.profileIcon.active,
    inactiveIcon: AppIcons.profileIcon.inactive,
    label: Strings.profileScreenTitle,
  ),
];
