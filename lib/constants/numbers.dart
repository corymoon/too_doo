class Numbers {
  static const double borderRadiusLarge = 30.0;
  static const double borderRadiusMedium = 20.0;
  static const double borderRadiusSmall = 12.0;

  static const double textFieldLabelLetterSpacing = 2.0;
  static const double mediumLetterSpacing = 4.0;
  static const double hugeLetterSpacing = 6.0;
}
