import 'dart:math';

import 'package:flutter/material.dart';

class Dracula {
  // functions
  static Color lightColor(Color color) => color.withOpacity(0.05);
  static Color transparentColor(Color color) => color.withOpacity(0.1);
  static LinearGradient gradient(List<Color> colors) => LinearGradient(
        colors: colors,
        transform: const GradientRotation(135 * pi / 180),
      );

  // black
  static const black = Color(0xFF21222C);
  static const blackSecondary = Color(0xFF414558);
  static const blackTernary = Color(0xFFA7ABBE);
  static final blackLight = lightColor(black);

  // white
  static const white = Color(0xFFF8F8F2);
  static const whiteSecondary = Color(0xFFFFFFFF);
  static final whiteLight = lightColor(white);

  // cyan
  static const cyan = Color(0xFF80FFEA);
  static const cyanSecondary = Color(0xFFCCFFF6);
  static final cyanLight = lightColor(cyan);
  static final cyan100 = cyan.withOpacity(0.1);
  static final cyan200 = cyan.withOpacity(0.2);
  static final cyan300 = cyan.withOpacity(0.3);
  static final cyan400 = cyan.withOpacity(0.4);
  static final cyan500 = cyan.withOpacity(0.5);
  static final cyan600 = cyan.withOpacity(0.6);
  static final cyan700 = cyan.withOpacity(0.7);
  static final cyan800 = cyan.withOpacity(0.8);
  static final cyan900 = cyan.withOpacity(0.9);

  // green
  static const green = Color(0xFF8AFF80);
  static const greenSecondary = Color(0xFFD0FFCC);
  static final greenLight = lightColor(green);
  static final green100 = green.withOpacity(0.1);
  static final green200 = green.withOpacity(0.2);
  static final green300 = green.withOpacity(0.3);
  static final green400 = green.withOpacity(0.4);
  static final green500 = green.withOpacity(0.5);
  static final green600 = green.withOpacity(0.6);
  static final green700 = green.withOpacity(0.7);
  static final green800 = green.withOpacity(0.8);
  static final green900 = green.withOpacity(0.9);

  // orange
  static const orange = Color(0xFFFFCA80);
  static const orangeSecondary = Color(0xFFFFEACC);
  static final orangeLight = lightColor(orange);
  static final orange100 = orange.withOpacity(0.1);
  static final orange200 = orange.withOpacity(0.2);
  static final orange300 = orange.withOpacity(0.3);
  static final orange400 = orange.withOpacity(0.4);
  static final orange500 = orange.withOpacity(0.5);
  static final orange600 = orange.withOpacity(0.6);
  static final orange700 = orange.withOpacity(0.7);
  static final orange800 = orange.withOpacity(0.8);
  static final orange900 = orange.withOpacity(0.9);

  // pink
  static const pink = Color(0xFFFF80BF);
  static const pinkSecondary = Color(0xFFFFCCE6);
  static final pinkLight = lightColor(pink);
  static final pink100 = pink.withOpacity(0.1);
  static final pink200 = pink.withOpacity(0.2);
  static final pink300 = pink.withOpacity(0.3);
  static final pink400 = pink.withOpacity(0.4);
  static final pink500 = pink.withOpacity(0.5);
  static final pink600 = pink.withOpacity(0.6);
  static final pink700 = pink.withOpacity(0.7);
  static final pink800 = pink.withOpacity(0.8);
  static final pink900 = pink.withOpacity(0.9);

  // purple
  static const purple = Color(0xFF9580FF);
  static const purpleSecondary = Color(0xFFD5CCFF);
  static final purpleLight = lightColor(purple);
  static final purple100 = purple.withOpacity(0.1);
  static final purple200 = purple.withOpacity(0.2);
  static final purple300 = purple.withOpacity(0.3);
  static final purple400 = purple.withOpacity(0.4);
  static final purple500 = purple.withOpacity(0.5);
  static final purple600 = purple.withOpacity(0.6);
  static final purple700 = purple.withOpacity(0.7);
  static final purple800 = purple.withOpacity(0.8);
  static final purple900 = purple.withOpacity(0.9);

  // red
  static const red = Color(0xFFFF9580);
  static const redSecondary = Color(0xFFFFD5CC);
  static final redLight = lightColor(red);
  static final red100 = red.withOpacity(0.1);
  static final red200 = red.withOpacity(0.2);
  static final red300 = red.withOpacity(0.3);
  static final red400 = red.withOpacity(0.4);
  static final red500 = red.withOpacity(0.5);
  static final red600 = red.withOpacity(0.6);
  static final red700 = red.withOpacity(0.7);
  static final red800 = red.withOpacity(0.8);
  static final red900 = red.withOpacity(0.9);

  // yellow
  static const yellow = Color(0xFFFFFF80);
  static const yellowSecondary = Color(0xFFFFCFFC);
  static final yellowLight = lightColor(yellow);
  static final yellow100 = yellow.withOpacity(0.1);
  static final yellow200 = yellow.withOpacity(0.2);
  static final yellow300 = yellow.withOpacity(0.3);
  static final yellow400 = yellow.withOpacity(0.4);
  static final yellow500 = yellow.withOpacity(0.5);
  static final yellow600 = yellow.withOpacity(0.6);
  static final yellow700 = yellow.withOpacity(0.7);
  static final yellow800 = yellow.withOpacity(0.8);
  static final yellow900 = yellow.withOpacity(0.9);

  // transparent
  static final cyanTransparent = transparentColor(cyan);
  static final greenTransparent = transparentColor(green);
  static final orangeTransparent = transparentColor(orange);
  static final pinkTransparent = transparentColor(pink);
  static final purpleTransparent = transparentColor(purple);
  static final redTransparent = transparentColor(red);
  static final yellowTransparent = transparentColor(yellow);

  static const disabled = Color(0xFF6C7393);
  static const gradientDegree = 135;
  static final glowColor = whiteSecondary.withOpacity(0.25);

  static const accentColor = purple;
  static final purpleCyan = gradient([purple, cyan]);
  static final yellowPink = gradient([yellow, pink]);
  static final cyanGreen = gradient([cyan, green]);
  static final pinkPurple = gradient([pink, purple]);
}
