import 'package:flutter/material.dart';

import '/constants/constants.dart';

class AppShapes {
  static const bottomSheetShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(Numbers.borderRadiusLarge),
      topRight: Radius.circular(Numbers.borderRadiusLarge),
    ),
  );

  static final allCornersLargeRadius = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(Numbers.borderRadiusLarge),
  );

  static final allCornersMediumRadius = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(Numbers.borderRadiusMedium),
  );

  static final allCornersSmallRadius = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(Numbers.borderRadiusSmall),
  );

  static const leftCornersBigRadius = BorderRadius.only(
    topLeft: Radius.circular(Numbers.borderRadiusLarge),
    bottomLeft: Radius.circular(Numbers.borderRadiusLarge),
  );

  static const rightCornersBigRadius = BorderRadius.only(
    topRight: Radius.circular(Numbers.borderRadiusLarge),
    bottomRight: Radius.circular(Numbers.borderRadiusLarge),
  );
}
