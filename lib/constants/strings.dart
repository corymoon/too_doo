class Strings {
  /// title in the app bar
  static const String appTitle = 'toodoo';

  /// title of the dialog to update a todo
  static const String updateTodoTitle = 'edit todo';

  /// button text on the dialog to update a todo
  static const String updateTodoButtonText = 'update';

  /// hint text for new todo text field
  static const String newTodoHintText = 'eg. walk the dog';

  /// title of the dialog to add a new todo
  static const String newTodoTitle = 'new todo';

  /// button text on the dialog to add a new todo
  static const String newTodoButtonText = 'add';

  /// login screen email text field hint
  static const String authScreenEmailHint = 'email';

  /// login screen password text field hint
  static const String authScreenPasswordHint = 'password';

  /// login screen button text
  static const String authScreenLoginButtonText = 'login';

  /// login screen title text
  static const String authScreenTitleText = 'LOGIN';

  /// login screen button text
  static const String signupScreenSignupButtonText = 'sign up';

  /// sign up screen title text
  static const String signupScreenTitleText = 'SIGNUP';

  ///
  static const String createAnAccountText = 'create an account';

  /// dismiss background delete text
  static const String dismissDeleteText = 'delete';

  /// bottom sheet menu complete text
  static const String completeText = 'complete';

  /// log out text
  static const String logOutText = 'log out';

  /// todos screen title
  static const String todosScreenTitle = 'todos';

  /// profile screen title
  static const String profileScreenTitle = 'profile';

  /// profile screen title
  static const String feedScreenTitle = 'feed';

  /// profile screen title
  static const String archiveScreenTitle = 'archive';

  /// display name label text
  static const String displayNameLabelText = 'display name';

  /// account screen email label text
  static const String emailLabelText = 'email';

  /// change photo text
  static const String changePhotoText = 'change photo';

  /// pick new photo source text
  static const String pickPhotoSourceText = 'select a source';

  /// pick photo gallery option text
  static const String pickPhotoGalleryText = 'gallery';

  /// pick photo camera option text
  static const String pickPhotoCameraText = 'camera';

  ///
  static const String deleteDoneConfirmMessage =
      'are you sure you want to delete all todos currently marked complete? you can\'t undo this...';

  static const String keyboardShortcutsText = 'keyboard shortcuts';
}
