import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppIcons {
  static const IconData appLogoIcon = Icons.check_circle_outline;

  static const IconData fabAddIcon = Icons.add;

  static const IconData logoutIcon = Icons.logout_rounded;

  static const IconData deleteIcon = Icons.delete_forever_rounded;

  static const List<IconData> authScreenIcons = [
    Icons.fact_check_rounded,
    Icons.check_circle_outline_rounded,
    FontAwesomeIcons.clipboardCheck,
  ];

  static const IconData emailFieldIcon = Icons.person;
  static const IconData passwordFieldIcon = Icons.lock;
  static const IconData verifyEmailIcon = Icons.mark_email_read_rounded;

  static const AppIconWithState passwordVisibilityIcon = AppIconWithState(
    active: Icons.visibility_rounded,
    inactive: Icons.visibility_off_rounded,
  );

  static const IconData appBarMenuIcon = Icons.menu_rounded;
  static const IconData overflowMenuIcon = Icons.more_vert_rounded;

  static const IconData accountDisplayNameIcon = Icons.badge_rounded;
  static const IconData accountEmailIcon = Icons.email_rounded;

  // profile photo source icons
  static const IconData pickPhotoIcon = Icons.add_a_photo_rounded;
  static const IconData pickPhotoGalleryIcon = Icons.photo_album_rounded;
  static const IconData pickPhotoCameraIcon = Icons.camera_rounded;

  // todos
  static const AppIconWithState todosIcon = AppIconWithState(
    active: Icons.check_circle,
    inactive: Icons.check_circle_outline,
  );

  // archive
  static const AppIconWithState archiveIcon = AppIconWithState(
    active: Icons.archive,
    inactive: Icons.archive_outlined,
  );

  // feed
  static const AppIconWithState feedIcon = AppIconWithState(
    active: Icons.feed_rounded,
    inactive: Icons.feed_outlined,
  );

  // profile
  static const AppIconWithState profileIcon = AppIconWithState(
    active: Icons.person_rounded,
    inactive: Icons.person_outline_rounded,
  );

  static const IconData defaultProfilePhotoIcon = Icons.account_circle_rounded;

  // todo states
  static const IconData todoArchiveStateIcon = Icons.archive_rounded;
  static const IconData todoUnarchiveStateIcon = Icons.unarchive_rounded;

  static const IconData todoCompleteStateIcon = Icons.done_rounded;
  static const IconData todoIncompleteStateIcon = Icons.remove_done_rounded;

  static const IconData todoPriorityStateIcon = Icons.star_rounded;
  static const IconData todoNotPriorityStateIcon = Icons.star_outline_rounded;

  static const IconData todoSnoozedStateIcon = Icons.dark_mode_rounded;
  static const IconData todoWakeStateIcon = Icons.light_mode_rounded;

  static const IconData todoActiveStateIcon = Icons.done_rounded;
  static const IconData todoDefaultStateIcon = Icons.circle_rounded;

  static const IconData todoPriorityWithDueDateIcon = Icons.notification_important_rounded;
  static const IconData todoWithDueDateIcon = Icons.notifications_active_rounded;
  static const IconData todoCreatedStateIcon = Icons.add_task_rounded;
  static const IconData todoUnknownStateIcon = Icons.help_rounded;

  //
  static const IconData keyboardShortcutsIcon = Icons.keyboard_rounded;
  static const IconData closeIcon = Icons.close_rounded;
  static const IconData cancelDueDateIcon = Icons.alarm_off_rounded;
  static const IconData helpIcon = Icons.help_rounded;
  static const IconData saveIcon = Icons.save_rounded;
  static const IconData notesIcon = Icons.notes_rounded;
}

class AppIconWithState {
  final IconData active;
  final IconData inactive;

  const AppIconWithState({required this.active, required this.inactive});
}
