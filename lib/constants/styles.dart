import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/themes/themes.dart';

class AppStyles {
  static TextStyle textFieldLabelStyle(Color color) {
    return TextStyle(
      color: color,
      fontWeight: FontWeight.bold,
      letterSpacing: Numbers.textFieldLabelLetterSpacing,
    );
  }

  static final TextStyle listTileTitleStyle = kAppTextTheme.subtitle1!;

  static final TextStyle listTileSubtitleStyle = kAppTextTheme.bodyText2!.copyWith(
    fontSize: 14.0,
    color: AppColors.textColor60,
  );
}
