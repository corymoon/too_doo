import 'package:flutter/material.dart';
import 'constants.dart';

import '/functions/create_material_color.dart';

class AppColors {
  static const Color pink = Dracula.pink;
  static const Color purple = Dracula.purple;
  static const Color green = Dracula.green;
  static const Color red = Dracula.red;
  static const Color blue = Dracula.cyan;

  static const Color textColor = Dracula.white;

  static final Color textColor12 = textColor.withOpacity(0.12);
  static final Color textColor20 = textColor.withOpacity(0.2);
  static final Color textColor30 = textColor.withOpacity(0.3);
  static final Color textColor40 = textColor.withOpacity(0.4);
  static final Color textColor50 = textColor.withOpacity(0.5);
  static final Color textColor60 = textColor.withOpacity(0.6);
  static final Color textColor70 = textColor.withOpacity(0.7);

  static final MaterialColor primaryColor = createMaterialColor(purple);
  static final Color primaryColorLight = Dracula.purple400;
  static final Gradient primaryGradient = Dracula.pinkPurple;
  static const Color onPrimary = textColor;

  static MaterialColor primarySurfaceColor = createMaterialColor(Dracula.black);

  static Color topSurfaceBackgroundColor = primarySurfaceColor.shade200;
  static Color elevatedSurfaceBackgroundColor = primarySurfaceColor.shade400;
  static Color backgroundColor = primarySurfaceColor.shade600;

  static Color errorColor = red;
  static Color onError = getOnColor(errorColor);

  // static Color textFieldFillColor = elevatedSurfaceBackgroundColor;
  static Color textFieldFillColor = Dracula.blackSecondary;
  static Color textFieldLabelColor = primaryColor;

  static Color archiveColor = blue;
  static Color onArchive = getOnColor(archiveColor);

  static Color completeColor = green;
  static Color onComplete = getOnColor(completeColor);

  static Color activeColor = purple;
  static Color onActive = getOnColor(activeColor);

  static Color snoozedColor = pink;
  static Color onSnoozed = getOnColor(snoozedColor);

  static Color snoozedTextColor = textColor20;

  static Color priorityColor = Dracula.orange;
  static Color onPriority = getOnColor(priorityColor);

  static Color getOnColor(Color color) =>
      color.computeLuminance() >= 0.5 ? Dracula.black : Dracula.white;

  static Color textColorOpacity(int level) => textColor.withOpacity(level / 100);
}
