import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class AppUser {
  final String uid;
  final String? email;
  final String? photoURL;
  final String? displayName;
  final int? activeTodos;
  final int? archivedTodos;
  final int? completeTodos;
  final int? snoozedTodos;
  final DateTime? lastActivity;

  AppUser({
    required this.uid,
    this.email,
    this.displayName,
    this.photoURL,
    this.activeTodos,
    this.archivedTodos,
    this.completeTodos,
    this.snoozedTodos,
    this.lastActivity,
  });

  AppUser copyWith({
    String? uid,
    String? email,
    String? photoURL,
    String? displayName,
    int? activeTodos,
    int? archivedTodos,
    int? completeTodos,
    int? snoozedTodos,
    DateTime? lastActivity,
  }) {
    return AppUser(
      uid: uid ?? this.uid,
      email: email ?? this.email,
      photoURL: photoURL ?? this.photoURL,
      displayName: displayName ?? this.displayName,
      activeTodos: activeTodos ?? this.activeTodos,
      archivedTodos: archivedTodos ?? this.archivedTodos,
      completeTodos: completeTodos ?? this.completeTodos,
      snoozedTodos: snoozedTodos ?? this.snoozedTodos,
      lastActivity: lastActivity ?? this.lastActivity,
    );
  }

  factory AppUser.fromFirestore(Map<String, dynamic> data) {
    var _lastActivity = data['lastActivity'] as Timestamp;
    return AppUser(
      uid: data['uid'],
      email: data['email'],
      displayName: data['displayName'],
      photoURL: data['photoURL'],
      activeTodos: data['activeTodos'],
      archivedTodos: data['archivedTodos'],
      completeTodos: data['completeTodos'],
      snoozedTodos: data['snoozedTodos'],
      lastActivity: _lastActivity.toDate(),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'photoURL': photoURL,
      'displayName': displayName,
      'activeTodos': activeTodos,
      'archivedTodos': archivedTodos,
      'completeTodos': completeTodos,
      'snoozedTodos': snoozedTodos,
      'lastActivity': lastActivity,
    };
  }

  String get lastActivityString => DateFormat.yMd().add_jm().format(lastActivity!);
}
