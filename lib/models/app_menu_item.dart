import 'package:flutter/material.dart';

class AppMenuItem {
  final int index;
  final IconData activeIcon;
  final IconData inactiveIcon;
  final String label;

  AppMenuItem({
    required this.index,
    required this.activeIcon,
    required this.inactiveIcon,
    required this.label,
  });
}
