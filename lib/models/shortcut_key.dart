class ShortcutKey {
  final String key;
  final String action;

  ShortcutKey({required this.key, required this.action});
}

final kAppShortcutKeys = [
  ShortcutKey(key: 'n', action: 'new todo'),
  ShortcutKey(key: 't', action: 'navigate to todos list'),
  ShortcutKey(key: 'a', action: 'navigate to archive'),
  ShortcutKey(key: 'f', action: 'navigate to feed'),
  ShortcutKey(key: 'p', action: 'navigate to profile'),
];
