import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:duration/duration.dart';
import 'package:intl/intl.dart';

import '/enums/enums.dart';
import '/extensions/extensions.dart';

class Todo {
  String uid;
  String title;
  DateTime created;
  DateTime completedAt;
  TodoState state;
  String? notes;
  DateTime dueDate;
  bool? saved;

  Todo({
    required this.uid,
    required this.title,
    required this.created,
    required this.completedAt,
    required this.state,
    required this.dueDate,
    this.notes,
    this.saved,
  });

  String get createdDateString => DateFormat.yMd().format(created);
  String get createdTimeString => DateFormat.jm().format(created);

  String get completedDateString => DateFormat.yMd().format(completedAt);
  String get completedTimeString => DateFormat.jm().format(completedAt);

  String get displayDate => '$createdDateString @ $createdTimeString';
  String get completedDisplay => '$completedDateString @ $completedTimeString';

  String get archiveDisplayDate {
    if (completedAt == DateTime(1970, 1, 1)) {
      return displayDate;
    } else {
      return completedDisplay;
    }
  }

  String get dueDisplayDate {
    String dateString = DateFormat.yMd().format(dueDate);
    String timeString = DateFormat.jm().format(dueDate);
    return '$dateString @ $timeString';
  }

  String? get duration {
    if (completedAt != DateTime(1970, 1, 1)) {
      String timeTaken = printDuration(
        completedAt.difference(created),
        tersity: DurationTersity.minute,
      );
      return 'took $timeTaken';
    } else {
      return null;
    }
  }

  Todo copyWith({
    String? title,
    DateTime? created,
    DateTime? completedAt,
    TodoState? state,
    String? notes,
    DateTime? dueDate,
    bool? saved,
  }) {
    return Todo(
      uid: uid,
      title: title ?? this.title,
      created: created ?? this.created,
      completedAt: completedAt ?? this.completedAt,
      state: state ?? this.state,
      notes: notes ?? this.notes,
      dueDate: dueDate ?? this.dueDate,
      saved: saved ?? this.saved,
    );
  }

  factory Todo.fromFirestore(Map<String, dynamic> data, String uid) {
    Timestamp date = data['created'] as Timestamp;
    Timestamp completed = data['completedAt'] as Timestamp;
    Timestamp dueDate = data['dueDate'] as Timestamp;
    String state = data['state'];
    bool saved = data['saved'] ?? false;

    return Todo(
      title: data['title'],
      uid: uid,
      created: date.toDate(),
      completedAt: completed.toDate(),
      state: state.toTodoState(),
      notes: data['notes'],
      dueDate: dueDate.toDate(),
      saved: saved,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'title': title,
      'created': created,
      'completedAt': completedAt,
      'state': state.asString(),
      'notes': notes,
      'dueDate': dueDate,
      'saved': saved,
    };
  }
}
