import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '/constants/constants.dart';

enum FeedItemType {
  created,
  completed,
  incomplete,
  snoozed,
  wake,
  archived,
  unarchive,
  priority,
  unpriority,
  none,
}

extension FeedItemTypeFromString on String {
  FeedItemType toFeedItemType() {
    switch (this) {
      case 'created':
        return FeedItemType.created;
      case 'completed':
        return FeedItemType.completed;
      case 'incomplete':
        return FeedItemType.incomplete;
      case 'snoozed':
        return FeedItemType.snoozed;
      case 'wake':
        return FeedItemType.wake;
      case 'archived':
        return FeedItemType.archived;
      case 'unarchive':
        return FeedItemType.unarchive;
      case 'priority':
        return FeedItemType.priority;
      case 'unpriority':
        return FeedItemType.unpriority;
      default:
        return FeedItemType.none;
    }
  }
}

extension FeedItemTypeString on FeedItemType {
  String asString() {
    const _todoName = 'toodoo';
    switch (this) {
      case FeedItemType.created:
        return 'created a new $_todoName';
      case FeedItemType.completed:
        return 'completed a $_todoName';
      case FeedItemType.incomplete:
        return 'marked a $_todoName incomplete';
      case FeedItemType.snoozed:
        return 'snoozed a $_todoName';
      case FeedItemType.wake:
        return 'woke a $_todoName';
      case FeedItemType.archived:
        return 'archived a $_todoName';
      case FeedItemType.unarchive:
        return 'removed a $_todoName from the archive';
      case FeedItemType.priority:
        return 'marked a $_todoName as priority';
      case FeedItemType.unpriority:
        return 'removed priority from a $_todoName';
      case FeedItemType.none:
      default:
        return 'did something';
    }
  }
}

class FeedItem {
  final String authorName;
  final String authorPhotoURL;
  final FeedItemType feedItemType;
  final DateTime date;
  final String uid;
  final String authorId;

  const FeedItem({
    required this.uid,
    required this.authorId,
    required this.authorName,
    required this.authorPhotoURL,
    required this.date,
    required this.feedItemType,
  });

  factory FeedItem.fromFirestore(Map<String, dynamic> data, String uid) {
    Timestamp date = data['date'] as Timestamp;
    String type = data['feedItemType'] as String;

    return FeedItem(
      authorName: data['authorName'],
      authorPhotoURL: data['authorPhotoURL'],
      authorId: data['authorId'],
      date: date.toDate(),
      feedItemType: type.toFeedItemType(),
      uid: uid,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'authorName': authorName,
      'authorPhotoURL': authorPhotoURL,
      'date': date,
      'feedItemType': feedItemType,
      'authorId': authorId,
    };
  }

  String get dateString => DateFormat.yMd().format(date);
  String get timeString => DateFormat.jm().format(date);

  String get displayDate {
    final now = DateTime.now();

    final isSameDay = date.year == now.year && date.month == now.month && date.day == now.day;

    if (isSameDay) {
      return timeago.format(now.subtract(now.difference(date)), locale: 'en_short');
    } else {
      return DateFormat.MMMd().format(date);
      // return '$dateString @ $timeString';
    }
  }

  Color get onColor {
    return AppColors.getOnColor(color);
  }

  Color get color {
    switch (feedItemType) {
      case FeedItemType.created:
        return AppColors.activeColor;

      case FeedItemType.incomplete:
      case FeedItemType.completed:
        return AppColors.completeColor;

      case FeedItemType.snoozed:
      case FeedItemType.wake:
        return AppColors.snoozedColor;

      case FeedItemType.archived:
      case FeedItemType.unarchive:
        return AppColors.archiveColor;

      case FeedItemType.priority:
      case FeedItemType.unpriority:
        return AppColors.priorityColor;

      case FeedItemType.none:
      default:
        return AppColors.errorColor;
    }
  }

  IconData get icon {
    switch (feedItemType) {
      case FeedItemType.created:
        return AppIcons.todoCreatedStateIcon;
      case FeedItemType.incomplete:
        return AppIcons.todoIncompleteStateIcon;

      case FeedItemType.completed:
        return AppIcons.todoActiveStateIcon;

      case FeedItemType.snoozed:
        return AppIcons.todoSnoozedStateIcon;
      case FeedItemType.wake:
        return AppIcons.todoWakeStateIcon;

      case FeedItemType.archived:
        return AppIcons.todoArchiveStateIcon;
      case FeedItemType.unarchive:
        return AppIcons.todoUnarchiveStateIcon;

      case FeedItemType.priority:
        return AppIcons.todoPriorityStateIcon;
      case FeedItemType.unpriority:
        return AppIcons.todoNotPriorityStateIcon;

      case FeedItemType.none:
      default:
        return AppIcons.todoUnknownStateIcon;
    }
  }
}
