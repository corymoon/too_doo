import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart' as md;
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/extensions.dart';
import '/models/models.dart';
import '/screens/screens.dart';
import '/services/services.dart';
import '/widgets/widgets.dart';
import 'todos_screen_components.dart';

class TodoListTile extends StatelessWidget {
  final Todo todo;
  final DatabaseService database;
  TodoListTile({
    Key? key,
    required this.todo,
    required this.database,
  }) : super(key: key);

  final TextEditingController titleController = TextEditingController();
  final TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    titleController.text = todo.title;
    dateController.text = todo.completedAt.toString();
    final ShortcutsService shortcutsService = context.read<ShortcutsService>();
    return ListTile(
      leading: TodoCheckbox(database: database, todo: todo),
      title: TodoTitle(todo: todo),
      subtitle: TodoSubtitle(todo: todo),
      onTap: () async {
        shortcutsService.disableShortcuts();
        await context.navigator.push(
          PageTransition(
            child: TodoDetailScreen(
              database: database,
              todo: todo,
            ),
            type: PageTransitionType.bottomToTop,
          ),
        );
        shortcutsService.enableShortcuts();
      },
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (todo.hasNotes) TodoViewNotesButton(todo: todo),
          _buildInteractionMenu(context),
        ],
      ),
    );
  }

  Widget _buildInteractionMenu(BuildContext context) {
    return PopupMenuButton(
      padding: const EdgeInsets.all(2.0),
      iconSize: 20.0,
      elevation: 0.0,
      color: AppColors.elevatedSurfaceBackgroundColor,
      icon: Icon(
        AppIcons.overflowMenuIcon,
        color: AppColors.textColor70,
      ),
      shape: AppShapes.allCornersLargeRadius,
      tooltip: 'show menu',
      itemBuilder: (context) {
        return [
          if (todo.hasDueDate)
            PopupMenuItem(
              onTap: () async => database.updateTodo(
                todo: todo,
                dueDate: DateTime(1970),
              ),
              child: Row(
                children: const [
                  Icon(AppIcons.cancelDueDateIcon),
                  Space.horizontalSmall,
                  Text('remove due date'),
                ],
              ),
            ),
          PopupMenuItem(
            onTap: () async => database.updateTodo(
              todo: todo,
              state: todo.isPriority ? TodoState.active : TodoState.priority,
            ),
            child: ListTileMenuItem(
              state: TodoState.priority,
              todoState: todo.isPriority,
              activeString: 'remove priority',
              inactiveString: 'priority',
            ),
          ),
          PopupMenuItem(
            onTap: () async => database.archiveTodo(todo),
            child: ListTileMenuItem(
              state: TodoState.archived,
              todoState: todo.isArchived,
              activeString: 'unarchive',
              inactiveString: 'archive',
            ),
          ),
          PopupMenuItem(
            onTap: () => _toggleSnoozed(context),
            // child: _buildMenuItem(TodoState.snoozed, todo.isSnoozed, 'wake', 'snooze'),
            child: ListTileMenuItem(
              state: TodoState.snoozed,
              todoState: todo.isSnoozed,
              activeString: 'wake',
              inactiveString: 'snooze',
            ),
          ),
          PopupMenuItem(
            onTap: () async => await database.removeTodo(todo),
            child: Row(
              children: const [
                Icon(AppIcons.deleteIcon),
                Space.horizontalSmall,
                Text('delete'),
              ],
            ),
          ),
        ];
      },
    );
  }

  // ignore: unused_element
  Widget _buildWebInteractionButtons(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: Icon(
            AppIcons.todoArchiveStateIcon,
            color: AppColors.archiveColor,
          ),
          onPressed: () async => database.archiveTodo(todo),
        ),
        IconButton(
          icon: Icon(
            todo.state == TodoState.snoozed
                ? AppIcons.todoWakeStateIcon
                : AppIcons.todoSnoozedStateIcon,
            color: AppColors.snoozedColor,
          ),
          onPressed: () => _toggleSnoozed(context),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(
            AppIcons.deleteIcon,
            color: AppColors.errorColor,
          ),
        ),
      ],
    );
  }

  Future<void> _toggleSnoozed(BuildContext context) async {
    if (todo.state == TodoState.complete) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: const Text('you can\'t snooze a complete todo'),
          actions: [
            TextButton(
              child: const Text('ok'),
              onPressed: () => context.navigator.pop(),
            )
          ],
        ),
      );
    } else {
      if (todo.state == TodoState.snoozed) {
        await database.wakeTodo(todo);
      } else {
        await database.snoozeTodo(todo);
      }
    }
  }

  // ignore: unused_element
  Future<void> _presentEditDialog(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (context) => EnterTodoDialog(
        title: Strings.updateTodoTitle,
        controller: titleController,
        onSubmit: () async {
          await database.updateTodo(
            todo: todo,
            title: titleController.text.trim(),
          );
          context.navigator.pop();
          titleController.clear();
        },
        submitText: Strings.updateTodoButtonText,
      ),
    );
  }
}
