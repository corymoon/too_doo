import 'package:flutter/material.dart';

import '/constants/constants.dart';

class TodosScreenFloatingActionButton extends StatelessWidget {
  const TodosScreenFloatingActionButton({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    // return FloatingActionButton(
    //   shape: AppShapes.allCornersLargeRadius,
    //   child: const Icon(
    //     AppIcons.fabAddIcon,
    //     color: AppColors.textColor,
    //   ),
    //   onPressed: onPressed,
    // );
    return FloatingActionButton(
      child: Container(
        width: 60,
        height: 60,
        child: const Icon(
          AppIcons.fabAddIcon,
          // size: 40,
          color: AppColors.textColor,
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: AppColors.primaryGradient,
        ),
      ),
      onPressed: onPressed,
    );
  }
}
