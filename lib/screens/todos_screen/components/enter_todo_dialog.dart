import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/models/models.dart';
import '/themes/themes.dart';

class EnterTodoDialog extends StatelessWidget {
  final String title;
  final TextEditingController controller;
  final Todo? todo;
  final VoidCallback onSubmit;
  final String submitText;

  const EnterTodoDialog({
    Key? key,
    this.todo,
    required this.title,
    required this.controller,
    required this.onSubmit,
    required this.submitText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      actionsPadding: const EdgeInsets.only(bottom: 20.0),
      content: TextField(
        controller: controller,
        autofocus: true,
        style: kAppTextTheme.subtitle1!.copyWith(color: AppColors.textColor),
        onSubmitted: (_) => onSubmit(),
        decoration: InputDecoration(
          label: const Text('title'),
          labelStyle: AppStyles.textFieldLabelStyle(AppColors.primaryColor),
          fillColor: AppColors.textFieldFillColor,
          hintText: todo == null ? Strings.newTodoHintText : todo!.title,
          hintStyle: kAppTextTheme.subtitle1!.copyWith(
            color: AppColors.textColor70,
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ElevatedButton(
            onPressed: onSubmit,
            child: SizedBox(
              width: double.infinity,
              child: Text(
                submitText,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
