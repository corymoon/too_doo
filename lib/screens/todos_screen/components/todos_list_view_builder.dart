import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/models/models.dart';
import '/services/services.dart';
import 'todos_screen_components.dart';

class TodosListViewBuilder extends StatelessWidget {
  const TodosListViewBuilder({
    Key? key,
    required this.database,
    required this.items,
  }) : super(key: key);

  final List<Todo> items;
  final DatabaseService database;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      padding: AppPadding.listViewPassing,
      itemCount: items.length,
      itemBuilder: (_, index) {
        return TodoListTile(
          todo: items[index],
          database: database,
        );
      },
      separatorBuilder: (_, __) => AppWidgets.divider,
    );
  }
}
