import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/extensions/extensions.dart';
import '/functions/functions.dart';
import '/models/models.dart';
import '/screens/screens.dart';
import '/services/services.dart';
import 'components/todos_screen_components.dart';

class TodosScreen extends StatelessWidget {
  final User? user;
  TodosScreen(this.user, {Key? key}) : super(key: key);

  final TextEditingController titleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final database = DatabaseService(user!.uid);
    return Scaffold(
      body: StreamBuilder<List<Todo>>(
        initialData: const [],
        stream: database.listTodos(),
        builder: (context, snapshot) {
          final items = snapshot.data;

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LoadingScreen();
          }

          if (items!.isEmpty) {
            return const NoContentScreen();
          }

          // sort by created date
          items.sort((a, b) => b.created.compareTo(a.created));
          // then sort by completed status
          items.sort((a, b) => b.isComplete ? 1 : -1);
          // then sort by snoozed status
          items.sort((a, b) => b.isSnoozed ? -1 : 1);
          // then sort by priority status
          items.sort((a, b) => b.isPriority ? 1 : -1);

          return TodosListViewBuilder(
            items: items.where((t) => !t.isArchived).toList(),
            database: database,
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: _buildFloatingActionButton(context, database),
    );
  }

  Widget? _buildFloatingActionButton(BuildContext context, DatabaseService database) {
    if (isLargeScreen(context)) {
      return null;
    } else {
      return TodosScreenFloatingActionButton(
        onPressed: () => showDialog(
          context: context,
          builder: (context) => EnterTodoDialog(
            title: Strings.newTodoTitle,
            controller: titleController,
            onSubmit: () => createNewTodo(context, database, titleController),
            submitText: Strings.newTodoButtonText,
          ),
        ),
      );
    }
  }
}
