import 'package:flutter/material.dart';

import '/constants/constants.dart';

class TodoTitleTextField extends StatelessWidget {
  const TodoTitleTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        labelText: 'title',
        fillColor: AppColors.textFieldFillColor,
        labelStyle: AppStyles.textFieldLabelStyle(AppColors.primaryColor),
        hintStyle: const TextStyle(
          color: AppColors.textColor,
        ),
      ),
      style: const TextStyle(
        color: AppColors.textColor,
      ),
    );
  }
}
