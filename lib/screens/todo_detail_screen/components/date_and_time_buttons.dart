import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/themes/themes.dart';

class DateAndTimeButtons extends StatelessWidget {
  const DateAndTimeButtons({
    Key? key,
    required this.width,
    required this.fieldPadding,
    required this.dateString,
    required this.timeString,
    required this.onUpdateDate,
    required this.onUpdateTime,
  }) : super(key: key);

  final double width;
  final double fieldPadding;
  final String dateString;
  final String timeString;
  final VoidCallback onUpdateDate;
  final VoidCallback onUpdateTime;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: width * 0.5,
          child: ElevatedButton(
            child: Text(dateString),
            style: _makeButtonStyle(AppShapes.leftCornersBigRadius),
            onPressed: onUpdateDate,
          ),
        ),
        SizedBox(width: fieldPadding),
        SizedBox(
          width: width * 0.5,
          child: ElevatedButton(
            child: Text(timeString),
            style: _makeButtonStyle(AppShapes.rightCornersBigRadius),
            onPressed: onUpdateTime,
          ),
        ),
      ],
    );
  }

  ButtonStyle _makeButtonStyle(BorderRadius borderRadius) {
    return ElevatedButton.styleFrom(
      primary: AppColors.textFieldFillColor,
      padding: const EdgeInsets.all(16.0),
      textStyle: kAppTextTheme.button!.copyWith(fontWeight: FontWeight.bold),
      shape: RoundedRectangleBorder(
        borderRadius: borderRadius,
      ),
    );
  }
}
