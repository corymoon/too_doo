import 'package:flutter/material.dart';

import '/constants/constants.dart';

class TodoDetailAppBar extends StatelessWidget with PreferredSizeWidget {
  const TodoDetailAppBar({
    Key? key,
    required this.onUpdateTodo,
    required this.title,
  }) : super(key: key);
  final String title;
  final VoidCallback onUpdateTodo;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: [
        IconButton(
          onPressed: onUpdateTodo,
          icon: const Icon(AppIcons.saveIcon),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
