import 'package:flutter/material.dart';
import '/constants/constants.dart';

class DateSectionTitle extends StatelessWidget {
  const DateSectionTitle(this.title, {Key? key, this.color}) : super(key: key);
  final String title;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        color: color ?? AppColors.primaryColor,
        fontWeight: FontWeight.bold,
        letterSpacing: Numbers.textFieldLabelLetterSpacing,
      ),
    );
  }
}
