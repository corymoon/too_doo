import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/extensions.dart';
import '/functions/functions.dart';
import '/models/models.dart';
import '/services/services.dart';
import 'components/todo_detail_screen_components.dart';

class TodoDetailScreen extends StatefulWidget {
  const TodoDetailScreen({
    Key? key,
    required this.todo,
    required this.database,
  }) : super(key: key);
  final Todo todo;
  final DatabaseService database;

  @override
  State<TodoDetailScreen> createState() => _TodoDetailScreenState();
}

class _TodoDetailScreenState extends State<TodoDetailScreen> {
  late TextEditingController titleController;
  late TextEditingController notesController;
  late DateTime updatedDate;
  late TimeOfDay updatedTimeOfDay;
  late DateTime dueDate;
  late TimeOfDay dueTimeOfDay;
  late DateTime updatedCompleteDate;
  late TimeOfDay updatedCompletedTimeOfDay;
  late Size size;

  @override
  void initState() {
    titleController = TextEditingController();
    notesController = TextEditingController();
    setState(() {
      updatedDate = widget.todo.created;
      updatedTimeOfDay = TimeOfDay(
        hour: widget.todo.created.hour,
        minute: widget.todo.created.minute,
      );

      updatedCompleteDate = widget.todo.completedAt;
      updatedCompletedTimeOfDay = TimeOfDay(
        hour: widget.todo.completedAt.hour,
        minute: widget.todo.completedAt.minute,
      );

      dueDate = widget.todo.dueDate;
      dueTimeOfDay = TimeOfDay(
        hour: widget.todo.dueDate.hour,
        minute: widget.todo.dueDate.minute,
      );
    });

    titleController.text = widget.todo.title;
    notesController.text = widget.todo.notes ?? '';
    super.initState();
  }

  @override
  void dispose() {
    titleController.dispose();
    notesController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _width = ResponsiveSize.screenSize(context);
    var _isMediumScreen = _width == ScreenSize.medium || _width == ScreenSize.large;
    var _isLargeScreen = _width == ScreenSize.extraLarge || _width == ScreenSize.extraExtraLarge;

    size = context.mediaQuery.size;
    double _sidePadding = _isLargeScreen
        ? 300.0
        : _isMediumScreen
            ? 150.0
            : 20.0;
    double _fieldPadding = 2.0;
    double _theWidth = size.width - (_sidePadding * 2) - _fieldPadding;

    return Scaffold(
      appBar: TodoDetailAppBar(
        title: (widget.todo.title),
        onUpdateTodo: () => _updateTodo(),
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: _sidePadding,
          right: _sidePadding,
          top: 20.0,
        ),
        child: Column(
          children: [
            TodoTitleTextField(controller: titleController),

            // - ENTERED DATE
            ..._buildEnteredDateSection(_theWidth, _fieldPadding),

            // - COMPLETED DATE
            if (widget.todo.completedAt != DateTime(1970, 1, 1))
              ..._buildCompletedDateTimeSection(_fieldPadding, _theWidth),

            // - DUE DATE

            if (!widget.todo.isSnoozed) ..._buildDueDateSection(_theWidth, _fieldPadding),

            // - NOTES
            ..._buildNotesField(),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildEnteredDateSection(double width, double fieldPadding) {
    return [
      Space.verticalLarge,
      const DateSectionTitle('entered'),
      Space.verticalMedium,
      DateAndTimeButtons(
        dateString: updatedDate.monthDayYear,
        timeString: updatedTimeOfDay.hourMinuteAmpm,
        fieldPadding: fieldPadding,
        width: width,
        onUpdateDate: () => _updateDate(),
        onUpdateTime: () => _updateTime(),
      )
    ];
  }

  List<Widget> _buildDueDateSection(double width, double fieldPadding) {
    return [
      Space.verticalLarge,
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          DateSectionTitle('due', color: AppColors.errorColor),
          if (widget.todo.hasDueDate)
            TextButton(
              child: const Icon(AppIcons.cancelDueDateIcon),
              style: TextButton.styleFrom(
                padding: const EdgeInsets.all(0.0),
                primary: AppColors.errorColor,
              ),
              onPressed: () async {
                context.navigator.pop();
                widget.database.updateTodo(
                  todo: widget.todo,
                  dueDate: DateTime(1970),
                );
              },
            ),
        ],
      ),
      Space.verticalMedium,
      DateAndTimeButtons(
        width: width,
        fieldPadding: fieldPadding,
        dateString: dueDate == DateTime(1970) ? '---' : dueDate.monthDayYear,
        timeString: dueDate == DateTime(1970) ? '---' : dueTimeOfDay.hourMinuteAmpm,
        onUpdateDate: () => _updateDueDate(),
        onUpdateTime: () => _updateDueTime(),
      ),
    ];
  }

  /// method to build notes field
  List<Widget> _buildNotesField() {
    return [
      Space.verticalLarge,
      Space.verticalLarge,
      TextField(
        controller: notesController,
        decoration: const InputDecoration(
          labelText: 'notes',
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        maxLines: null,
        minLines: null,
      ),
      Space.verticalSmall,
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Row(
          children: [
            Text(
              'markdown allowed',
              style: TextStyle(
                color: AppColors.textColor70,
              ),
            ),
            IconButton(
              icon: Icon(
                AppIcons.helpIcon,
                color: AppColors.textColor70,
              ),
              iconSize: 20.0,
              onPressed: () => launchMarkdownHelp(context),
            ),
          ],
        ),
      ),
    ];
  }

  /// method to build completed datetime section
  List<Widget> _buildCompletedDateTimeSection(double fieldPadding, double width) {
    return [
      Space.verticalLarge,
      const DateSectionTitle('completed'),
      Space.verticalMedium,
      DateAndTimeButtons(
        dateString: updatedCompleteDate.monthDayYear,
        timeString: updatedCompletedTimeOfDay.hourMinuteAmpm,
        fieldPadding: fieldPadding,
        width: width,
        onUpdateDate: () => _updateCompletedDate(),
        onUpdateTime: () => _updateCompletedTime(),
      ),
    ];
  }

  /// method to update date
  Future<void> _updateDate() async {
    // present picker and set date
    DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: widget.todo.created,
      firstDate: DateTime(1970, 1, 1),
      lastDate: DateTime(3000, 12, 31),
    );

    if (newDate != null) {
      setState(() {
        updatedDate = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          updatedTimeOfDay.hour,
          updatedTimeOfDay.minute,
        );
      });
    }
  }

  /// method to update due date
  Future<void> _updateDueDate() async {
    // present picker and set date
    DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(3000, 12, 31),
    );

    if (newDate != null) {
      setState(() {
        dueDate = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          dueTimeOfDay.hour,
          dueTimeOfDay.minute,
        );
      });
    }
  }

  /// method to update completed date
  Future<void> _updateCompletedDate() async {
    // present picker and set date
    DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: widget.todo.completedAt,
      firstDate: DateTime(1970, 1, 1),
      lastDate: DateTime(3000, 12, 31),
    );

    if (newDate != null) {
      setState(() {
        updatedCompleteDate = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          updatedCompletedTimeOfDay.hour,
          updatedCompletedTimeOfDay.minute,
        );
      });
    }
  }

  /// method to update time
  Future<void> _updateTime() async {
    // present picker and set date
    TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(
        hour: widget.todo.created.hour,
        minute: widget.todo.created.minute,
      ),
    );

    if (newTime != null) {
      setState(() {
        updatedTimeOfDay = TimeOfDay(
          hour: newTime.hour,
          minute: newTime.minute,
        );

        updatedDate = DateTime(
          updatedDate.year,
          updatedDate.month,
          updatedDate.day,
          newTime.hour,
          newTime.minute,
        );
      });
    }
  }

  /// method to update due date time
  Future<void> _updateDueTime() async {
    // present picker and set date
    TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(
        hour: widget.todo.dueDate.hour,
        minute: widget.todo.dueDate.minute,
      ),
    );

    if (newTime != null) {
      setState(() {
        dueTimeOfDay = TimeOfDay(
          hour: newTime.hour,
          minute: newTime.minute,
        );

        dueDate = DateTime(
          dueDate.year,
          dueDate.month,
          dueDate.day,
          newTime.hour,
          newTime.minute,
        );
      });
    }
  }

  /// method to update completed time
  Future<void> _updateCompletedTime() async {
    // present picker and set date
    TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(
        hour: widget.todo.completedAt.hour,
        minute: widget.todo.completedAt.minute,
      ),
    );

    if (newTime != null) {
      setState(() {
        updatedCompletedTimeOfDay = TimeOfDay(
          hour: newTime.hour,
          minute: newTime.minute,
        );

        updatedCompleteDate = DateTime(
          updatedCompleteDate.year,
          updatedCompleteDate.month,
          updatedCompleteDate.day,
          newTime.hour,
          newTime.minute,
        );
      });
    }
  }

  /// method to update todo
  Future<void> _updateTodo() async {
    context.navigator.pop();
    widget.database.updateTodo(
      todo: widget.todo,
      title: titleController.text.trim(),
      created: updatedDate,
      completedAt: updatedCompleteDate,
      notes: notesController.text,
      dueDate: dueDate,
    );
  }
}
