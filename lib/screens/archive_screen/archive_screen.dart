import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '/extensions/extensions.dart';
import '/models/models.dart';
import '/screens/screens.dart';
import '/services/services.dart';
import 'components/archive_screen_components.dart';

class ArchiveScreen extends StatelessWidget {
  final User? user;
  const ArchiveScreen(this.user, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final database = DatabaseService(user!.uid);
    return Scaffold(
      body: StreamBuilder<List<Todo>>(
        initialData: const [],
        stream: database.listArchivedTodos(),
        builder: (context, snapshot) {
          final items = snapshot.data;

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LoadingScreen();
          }

          if (items!.isEmpty) {
            return const NoContentScreen(
              text: 'nothing archived yet...',
            );
          }

          items.sort((a, b) => b.completedAt.compareTo(a.completedAt));
          return ArchivedListViewBuilder(
            items: items.where((todo) => todo.isArchived).toList(),
            database: database,
          );
        },
      ),
    );
  }
}
