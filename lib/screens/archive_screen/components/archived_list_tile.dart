import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/extensions.dart';
import '/models/models.dart';
import '/services/database_service.dart';
import '/widgets/widgets.dart';

class ArchivedListTile extends StatelessWidget {
  final Todo todo;
  final DatabaseService database;
  ArchivedListTile({
    Key? key,
    required this.todo,
    required this.database,
  }) : super(key: key);

  final TextEditingController titleController = TextEditingController();
  final TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    titleController.text = todo.title;
    dateController.text = todo.completedAt.toString();
    return ListTile(
      leading: todo.saved!
          ? SizedBox(
              width: 36.0,
              height: 36.0,
              child: Center(
                child: Icon(
                  AppIcons.saveIcon,
                  color: AppColors.textColor70,
                ),
              ),
            )
          : TodoCheckbox(database: database, todo: todo),
      title: TodoTitle(todo: todo),
      subtitle: TodoSubtitle(todo: todo),
      trailing: _buildInteractionMenu(context),
    );
  }

  Widget _buildInteractionMenu(BuildContext context) {
    return PopupMenuButton(
      padding: const EdgeInsets.all(2.0),
      iconSize: 20.0,
      elevation: 0.0,
      color: AppColors.elevatedSurfaceBackgroundColor,
      icon: Icon(
        AppIcons.overflowMenuIcon,
        color: AppColors.textColor70,
      ),
      shape: AppShapes.allCornersLargeRadius,
      tooltip: 'show menu',
      itemBuilder: (context) {
        return [
          PopupMenuItem(
            onTap: () async => await database.toggleSavedTodo(todo),
            child: Row(
              children: [
                const Icon(AppIcons.saveIcon),
                Space.horizontalSmall,
                Text(todo.saved! ? 'remove from saved' : 'save'),
              ],
            ),
          ),
          PopupMenuItem(
            onTap: todo.isArchived
                ? () async => database.unarchiveTodo(todo)
                : () async => database.archiveTodo(todo),
            child: ListTileMenuItem(
              state: TodoState.archived,
              todoState: todo.isArchived,
              activeString: 'unarchive',
              inactiveString: 'archive',
            ),
          ),
          PopupMenuItem(
            onTap: () async => await database.removeTodo(todo),
            child: Row(
              children: const [
                Icon(AppIcons.deleteIcon),
                Space.horizontalSmall,
                Text('delete'),
              ],
            ),
          ),
        ];
      },
    );
  }
}
