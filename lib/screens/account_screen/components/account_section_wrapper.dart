import 'package:flutter/material.dart';

import '/constants/constants.dart';

class AccountSectionWrapper extends StatelessWidget {
  const AccountSectionWrapper({
    Key? key,
    required this.child,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      padding: const EdgeInsets.all(20.0),
      width: double.infinity,
      decoration: BoxDecoration(
        // border: Border.all(color: AppColors.primaryColor, width: 1.0),
        border: Border.all(color: AppColors.textFieldFillColor, width: 1.0),
        borderRadius: BorderRadius.circular(Numbers.borderRadiusLarge),
      ),
      child: child,
    );
  }
}
