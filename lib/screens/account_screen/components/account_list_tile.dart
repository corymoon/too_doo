import 'package:flutter/material.dart';

import '/constants/constants.dart';

class AccountListTile extends StatelessWidget {
  final String value;
  final String label;
  final IconData? icon;
  final VoidCallback? onTapEdit;
  final Color? buttonColor;
  final Widget buttonChild;
  const AccountListTile({
    Key? key,
    required this.label,
    required this.value,
    this.icon,
    this.onTapEdit,
    this.buttonChild = const Text('edit'),
    this.buttonColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0),
      title: Text(
        label,
        style: TextStyle(
          color: AppColors.textColor50,
          fontSize: 14.0,
        ),
      ),
      subtitle: Text(
        value,
        style: const TextStyle(
          color: AppColors.textColor,
          fontSize: 18.0,
          fontWeight: FontWeight.w500,
        ),
      ),
      trailing: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: buttonColor ?? AppColors.textFieldFillColor,
        ),
        child: buttonChild,
        onPressed: onTapEdit,
      ),
    );
  }
}
