import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';
import '/widgets/widgets.dart';
import '/constants/constants.dart';
import '/models/models.dart';

class AccountHeader extends StatelessWidget {
  const AccountHeader({
    Key? key,
    required this.user,
    required this.onPickNewPhoto,
    required this.isLoading,
  }) : super(key: key);
  final AppUser user;
  final VoidCallback? onPickNewPhoto;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 10.0,
                  right: 10.0,
                ),
                child: UserAvatar(
                  name: user.displayName!,
                  photoURL: user.photoURL!,
                  radius: 60.0,
                ),
              ),
              // if (!kIsWeb)
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: Container(
                    padding: const EdgeInsets.all(4.0),
                    color: AppColors.backgroundColor,
                    child: FancyFab(
                      isLoading: isLoading,
                      child: const Icon(AppIcons.pickPhotoIcon),
                      onPressed: onPickNewPhoto,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Space.horizontalMedium,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                user.displayName!,
                style: const TextStyle(
                  color: AppColors.textColor,
                  fontSize: 28.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Space.verticalMedium,
              Chip(
                label: const Text(
                  'DOOER',
                  style: TextStyle(fontSize: 18.0),
                ),
                backgroundColor: AppColors.primaryColor,
              ),
            ],
          )
        ],
      ),
    );
  }
}
