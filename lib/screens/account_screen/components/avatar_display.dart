import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '/constants/colors.dart';

class AvatarDisplay extends StatelessWidget {
  final String photoURL;
  final String name;
  const AvatarDisplay({
    Key? key,
    required this.photoURL,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _radius = 80.0;
    double _textSize = _radius * 0.75;
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            child: CircleAvatar(
              radius: _radius,
              backgroundColor: AppColors.backgroundColor,
              backgroundImage: photoURL.isNotEmpty ? CachedNetworkImageProvider(photoURL) : null,
              child: photoURL.isEmpty
                  ? Text(
                      _getInitials(name),
                      style: TextStyle(
                        fontSize: _textSize,
                      ),
                    )
                  : null,
            ),
          ),
        ],
      ),
    );
  }

  String _getInitials(String name) {
    List<String> words = name.split(' ');
    List<String> initials = [];
    for (var word in words) {
      initials.add(word[0]);
    }
    return initials.join();
  }
}
