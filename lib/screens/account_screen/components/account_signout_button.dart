import 'package:flutter/material.dart';
import '/constants/constants.dart';

class AccountSignOutButton extends StatelessWidget {
  const AccountSignOutButton({
    Key? key,
    required this.onSignOutPressed,
  }) : super(key: key);
  final VoidCallback onSignOutPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          child: const Text(Strings.logOutText),
          onPressed: onSignOutPressed,
          style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.all(20.0),
            primary: AppColors.textFieldFillColor,
          ),
        ),
      ),
    );
  }
}
