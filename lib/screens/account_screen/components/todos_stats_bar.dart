import 'package:flutter/material.dart';

import '/constants/constants.dart';

class TodosStatsBar extends StatelessWidget {
  const TodosStatsBar({
    Key? key,
    required this.barColor,
    required this.maxValue,
    required this.value,
    required this.label,
  }) : super(key: key);
  final Color barColor;
  final int maxValue;
  final int value;
  final String label;

  @override
  Widget build(BuildContext context) {
    double _borderRadius = Numbers.borderRadiusMedium;
    double _height = 200.0;
    double _width = 20.0;

    double _barHeight = (value / maxValue) * _height;
    double _valuePadding = 8.0;
    double _valueHeight = 17.0;

    return Column(
      children: [
        SizedBox(
          height: _height + _valuePadding + _valueHeight,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0, right: 3.0),
                child: RotatedBox(
                  quarterTurns: -1,
                  child: Text(
                    label,
                    style: TextStyle(color: AppColors.textColor70),
                  ),
                ),
              ),
              Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  Container(
                    alignment: Alignment.bottomCenter,
                    height: _height,
                    width: _width,
                    decoration: BoxDecoration(
                      color: AppColors.textColor12,
                      borderRadius: BorderRadius.circular(_borderRadius),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        value.toString(),
                        style: TextStyle(
                          fontSize: _valueHeight - 1,
                          color: AppColors.textColor70,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: _valuePadding),
                      Container(
                        height: maxValue <= 0 ? 0 : _barHeight,
                        width: _width,
                        decoration: BoxDecoration(
                          color: barColor,
                          borderRadius: BorderRadius.circular(_borderRadius),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
