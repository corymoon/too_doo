import 'dart:io';

import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '/constants/constants.dart';
import '/extensions/extensions.dart';
import '/functions/functions.dart';
import '/models/models.dart';
import '/screens/screens.dart';
import '/services/services.dart';
import 'components/account_screen_components.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen(
    this.user, {
    Key? key,
  }) : super(key: key);
  final User? user;

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  bool _isLoading = false;
  TextEditingController displayNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    displayNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ImagePicker _picker = ImagePicker();
    return Scaffold(
      body: StreamBuilder<AppUser>(
        stream: DatabaseService(widget.user!.uid).getUserDoc(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LoadingScreen();
          }

          var appUser = snapshot.data!;
          displayNameController.text = appUser.displayName!;

          return SingleChildScrollView(
            child: Column(
              children: [
                AccountHeader(
                  user: appUser,
                  onPickNewPhoto: _isLoading ? null : () => _pickNewPhoto(appUser, _picker),
                  isLoading: _isLoading,
                ),
                Space.verticalMedium,
                AccountSectionWrapper(
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      AccountListTile(
                        label: 'display name',
                        value: appUser.displayName!,
                        onTapEdit: () => _startChangeDisplayName(appUser),
                      ),
                      AccountListTile(
                        label: 'email',
                        value: appUser.email!,
                        onTapEdit: widget.user!.emailVerified ? null : () => _startVerifyEmail(),
                        buttonChild: widget.user!.emailVerified
                            ? const Icon(AppIcons.verifyEmailIcon)
                            : const Text('verify'),
                      ),
                      AccountListTile(
                        label: 'password',
                        value: '••••••••',
                        buttonChild: const Text('change'),
                        onTapEdit: () => _startChangePassword(),
                      ),
                    ],
                  ),
                ),
                Space.verticalMedium,
                AccountSectionWrapper(
                  child: StreamBuilder<List<Todo>>(
                    stream: DatabaseService(widget.user!.uid).listArchivedTodos(),
                    builder: (context, snapshot) {
                      var todos = snapshot.data;
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      }
                      if (todos != null) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TodosStatsBar(
                              barColor: AppColors.priorityColor,
                              maxValue: todos.length,
                              value: todos.where((t) => t.isPriority).toList().length,
                              label: 'priority',
                            ),
                            TodosStatsBar(
                              barColor: AppColors.completeColor,
                              maxValue: todos.length,
                              value:
                                  todos.where((t) => t.isComplete && !t.isArchived).toList().length,
                              label: 'complete',
                            ),
                            TodosStatsBar(
                              barColor: AppColors.activeColor,
                              maxValue: todos.length,
                              value: todos.where((t) => t.isActive).toList().length,
                              label: 'active',
                            ),
                            TodosStatsBar(
                              barColor: AppColors.snoozedColor,
                              maxValue: todos.length,
                              value: todos.where((t) => t.isSnoozed).toList().length,
                              label: 'snoozed',
                            ),
                            TodosStatsBar(
                              barColor: AppColors.archiveColor,
                              maxValue: todos.length,
                              value: todos.where((t) => t.isArchived).toList().length,
                              label: 'archived',
                            ),
                          ],
                        );
                      } else {
                        return const Center(child: CircularProgressIndicator());
                      }
                    },
                  ),
                ),
                Space.verticalMedium,
                AccountSignOutButton(
                  onSignOutPressed: () => context.read<AuthService>().signOut(context),
                ),
                Space.verticalXLarge,
              ],
            ),
          );
        },
      ),
    );
  }

  /// method to update the user's display name
  Future<void> _updateUserDisplayName(AppUser appUser, String name) async {
    await DatabaseService(widget.user!.uid).updateUserDoc(
      appUser.copyWith(displayName: name),
    );
    context.navigator.pop();
  }

  /// method to prompt the user for a new photo, and then update it
  Future<void> _pickNewPhoto(AppUser appUser, ImagePicker picker) async {
    if (kIsWeb) {
      await _uploadNewPhoto(
        ImageSource.gallery,
        picker,
        appUser,
      );
    } else {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          actionsAlignment: MainAxisAlignment.center,
          contentPadding: const EdgeInsets.only(top: 20.0),
          content: const Text(
            Strings.pickPhotoSourceText,
            textAlign: TextAlign.center,
          ),
          actions: [
            TextButton.icon(
              icon: const Icon(AppIcons.pickPhotoGalleryIcon),
              label: const Text(Strings.pickPhotoGalleryText),
              onPressed: () async {
                context.navigator.pop();
                await _uploadNewPhoto(
                  ImageSource.gallery,
                  picker,
                  appUser,
                );
              },
            ),
            TextButton.icon(
              icon: const Icon(AppIcons.pickPhotoCameraIcon),
              label: const Text(Strings.pickPhotoCameraText),
              onPressed: () async {
                context.navigator.pop();
                await _uploadNewPhoto(
                  ImageSource.camera,
                  picker,
                  appUser,
                );
              },
            ),
          ],
        ),
      );
    }
  }

  /// method to upload new photo
  Future<void> _uploadNewPhoto(ImageSource source, ImagePicker picker, AppUser user) async {
    _setLoading(true);
    try {
      final _pickedImage = await picker.pickImage(
        source: source,
        maxHeight: 200.0,
        imageQuality: 70,
      );
      Reference ref = _userReference();

      if (kIsWeb) {
        if (_pickedImage != null) {
          var bytes = await _pickedImage.readAsBytes();
          await ref.putData(
            bytes,
            SettableMetadata(contentType: 'image/jpg'),
          );
        }
      } else {
        await ref.putFile(File(_pickedImage!.path));
      }

      String _downloadURL = await ref.getDownloadURL();

      await DatabaseService(widget.user!.uid).updateUserDoc(
        user.copyWith(photoURL: _downloadURL),
      );

      _setLoading(false);
    } on FirebaseException catch (e) {
      showSnackbarError(context, text: e.message.toString());
      _setLoading(false);
    } catch (e) {
      showSnackbarError(context, text: e.toString());
      _setLoading(false);
    }
  }

  /// function to set loading state
  void _setLoading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  /// method to get the reference for the current user
  Reference _userReference() {
    return FirebaseStorage.instance.ref().child('users').child('${widget.user!.uid}.jpg');
  }

  /// method to start the display name change process
  Future<void> _startChangeDisplayName(AppUser appUser) async {
    await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: TextFormField(
          autofocus: true,
          controller: displayNameController,
          onFieldSubmitted: (_) => _updateUserDisplayName(
            appUser,
            displayNameController.text.trim(),
          ),
          decoration: InputDecoration(
            labelText: 'display name',
            labelStyle: AppStyles.textFieldLabelStyle(AppColors.textFieldLabelColor),
          ),
        ),
        actions: [
          TextButton(
            child: const Text('cancel'),
            onPressed: () => context.navigator.pop(),
          ),
          TextButton(
            child: const Text('save'),
            onPressed: _isLoading
                ? null
                : () => _updateUserDisplayName(appUser, displayNameController.text.trim()),
          ),
        ],
      ),
    );
  }

  /// method to start the password change process
  Future<void> _startChangePassword() async {
    bool shouldResetPassword = await showConfirmationDialog(
      context,
      message: 'do you really want to change your password?',
    );
    if (shouldResetPassword) {
      await context.read<AuthService>().sendPasswordReset(context);
    }
  }

  /// method to start the email verification process
  Future<void> _startVerifyEmail() async {
    _setLoading(true);
    await context.read<AuthService>().sendVerificationEmail(context);
    _setLoading(false);
  }
}
