import 'package:flutter/material.dart';

import '/constants/constants.dart';

class AuthEmailTextField extends StatelessWidget {
  const AuthEmailTextField({Key? key, required this.controller})
      : super(key: key);

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.emailAddress,
      style: const TextStyle(
        color: AppColors.textColor,
      ),
      decoration: InputDecoration(
        hintStyle: TextStyle(
          color: AppColors.textColor70,
        ),
        labelStyle: AppStyles.textFieldLabelStyle(AppColors.textColor70),
        fillColor: AppColors.primaryColor.withOpacity(0.2),
        prefixIcon: Icon(
          AppIcons.emailFieldIcon,
          color: AppColors.primaryColor,
        ),
        labelText: Strings.authScreenEmailHint,
      ),
    );
  }
}
