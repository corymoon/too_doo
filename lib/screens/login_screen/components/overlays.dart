import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:blobs/blobs.dart' as blob;
import 'package:flutter/material.dart';

import '/constants/colors.dart';

class TopLeftOverlay extends StatelessWidget {
  const TopLeftOverlay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = context.mediaQuery.size;
    return Stack(
      children: [
        Positioned(
          top: -70,
          left: -130,
          child: _primaryBlob(
            '16-5-35586',
            size.width > 1000 ? size.width * 0.3 : size.width * 0.8,
          ),
        ),
        Positioned(
          top: -20,
          left: -150,
          child: _primaryBlob(
            '16-6-9886',
            size.width > 1000 ? size.width * 0.25 : size.width * 0.7,
          ),
        ),
      ],
    );
  }
}

class BottomRightOverlay extends StatelessWidget {
  const BottomRightOverlay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = context.mediaQuery.size;
    return Stack(
      children: [
        Positioned(
          bottom: -70,
          right: -150,
          child: _primaryBlob(
            '13-6-6582',
            size.width > 1000 ? size.width * 0.3 : size.width * 0.8,
          ),
        ),
        Positioned(
          bottom: -90,
          right: -60,
          child: _primaryBlob(
            '14-5-260',
            size.width > 1000 ? size.width * 0.25 : size.width * 0.7,
          ),
        ),
      ],
    );
  }
}

Widget _primaryBlob(String id, double size) {
  return SizedBox(
    width: size,
    height: size,
    child: blob.Blob.fromID(
      id: [id],
      size: size,
      styles: blob.BlobStyles(
        color: AppColors.primaryColor.withOpacity(0.5),
      ),
    ),
  );
}
