import 'package:flutter/material.dart';

import '/constants/constants.dart';

class AuthPasswordTextField extends StatefulWidget {
  const AuthPasswordTextField({
    Key? key,
    required this.controller,
    required this.onSubmitted,
  }) : super(key: key);
  final TextEditingController controller;
  final void Function(String) onSubmitted;

  @override
  State<AuthPasswordTextField> createState() => _AuthPasswordTextFieldState();
}

class _AuthPasswordTextFieldState extends State<AuthPasswordTextField> {
  bool showPassword = false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onSubmitted: widget.onSubmitted,
      controller: widget.controller,
      keyboardType: TextInputType.visiblePassword,
      obscureText: !showPassword,
      style: const TextStyle(
        color: AppColors.textColor,
      ),
      decoration: InputDecoration(
        hintStyle: TextStyle(
          color: AppColors.textColor70,
        ),
        labelStyle: AppStyles.textFieldLabelStyle(AppColors.textColor70),
        fillColor: AppColors.primaryColor.withOpacity(0.2),
        prefixIcon: Icon(
          AppIcons.passwordFieldIcon,
          color: AppColors.primaryColor,
        ),
        labelText: Strings.authScreenPasswordHint,
        suffixIcon: IconButton(
          icon: Icon(
            showPassword
                ? AppIcons.passwordVisibilityIcon.inactive
                : AppIcons.passwordVisibilityIcon.active,
            color: AppColors.primaryColor,
          ),
          onPressed: () {
            setState(() {
              showPassword = !showPassword;
            });
          },
        ),
      ),
    );
  }
}
