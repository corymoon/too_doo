import 'package:flutter/material.dart';

import '/widgets/widgets.dart';

class AuthElevatedButton extends StatelessWidget {
  const AuthElevatedButton({
    Key? key,
    required this.text,
    required this.onPressed,
    required this.isLoading,
  }) : super(key: key);
  final VoidCallback? onPressed;
  final String text;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return GradientElevatedButton(
      isLoading: isLoading,
      onPressed: onPressed!,
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
      ),
      // child: Text(
      //   text,
      //   style: kAppTextTheme.button!.copyWith(
      //     fontWeight: FontWeight.bold,
      //     color: AppColors.onPrimary,
      //   ),
      // ),
      label: text,
    );
  }
}
