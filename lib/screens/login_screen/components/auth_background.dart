import 'package:flutter/material.dart';

import 'overlays.dart';

class AuthBackground extends StatelessWidget {
  const AuthBackground({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        const TopLeftOverlay(),
        const BottomRightOverlay(),
        child,
      ],
    );
  }
}
