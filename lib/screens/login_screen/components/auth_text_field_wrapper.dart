import 'package:flutter/material.dart';
import 'package:black_hole_flutter/black_hole_flutter.dart';

class AuthTextFieldWrapper extends StatelessWidget {
  final Widget child;
  const AuthTextFieldWrapper(
    this.child, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = context.mediaQuery.size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      padding: const EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 5.0,
      ),
      width: size.width * 0.8,
      child: child,
    );
  }
}
