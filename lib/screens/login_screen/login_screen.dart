import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/functions/functions.dart';
import '/services/services.dart';
import 'components/login_screen_components.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late Size size;
  bool _isLoading = false;

  AuthFormType _formType = AuthFormType.logIn;

  late TextEditingController emailController;
  late TextEditingController passwordController;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _width = ResponsiveSize.screenSize(context);
    var _isMediumScreen = _width == ScreenSize.medium || _width == ScreenSize.large;
    var _isLargeScreen = _width == ScreenSize.extraLarge || _width == ScreenSize.extraExtraLarge;

    Size size = context.mediaQuery.size;

    double _sidePadding = _isLargeScreen
        ? 300.0
        : _isMediumScreen
            ? 150.0
            : 20.0;
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SingleChildScrollView(
        child: SizedBox(
          width: double.infinity,
          height: size.height,
          child: AuthBackground(child: _buildForm(size, _sidePadding)),
        ),
      ),
    );
  }

  Widget _buildForm(Size size, double sidePadding) {
    return Padding(
      padding: EdgeInsets.only(left: sidePadding, right: sidePadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            Strings.appTitle,
            style: TextStyle(
              fontSize: 28.0,
              color: AppColors.textColor,
              fontWeight: FontWeight.bold,
              letterSpacing: Numbers.mediumLetterSpacing,
            ),
          ),
          Icon(
            AppIcons.authScreenIcons[1],
            color: AppColors.primaryColor.withOpacity(0.4),
            size: size.height * 0.3,
          ),
          AnimatedSwitcher(
            // transitionBuilder: (Widget child, Animation<double> animation) {
            //   return ScaleTransition(child: child, scale: animation);
            // },
            switchInCurve: Curves.easeInCubic,
            switchOutCurve: Curves.easeOutCubic,
            duration: const Duration(milliseconds: 250),
            child: Text(
              _formType == AuthFormType.logIn
                  ? Strings.authScreenTitleText
                  : Strings.signupScreenTitleText,
              key: ValueKey<String>(_formType.toString()),
              style: TextStyle(
                fontSize: 25.0,
                color: AppColors.textColor70,
                fontWeight: FontWeight.bold,
                letterSpacing: Numbers.hugeLetterSpacing,
              ),
            ),
          ),
          Space.verticalMedium,
          AuthTextFieldWrapper(
            AuthEmailTextField(controller: emailController),
          ),
          AuthTextFieldWrapper(
            AuthPasswordTextField(
              controller: passwordController,
              onSubmitted: _formType == AuthFormType.logIn ? (_) => _signIn() : (_) => _signUp(),
            ),
          ),
          AuthTextFieldWrapper(
            _formType == AuthFormType.logIn
                ? AuthElevatedButton(
                    text: Strings.authScreenLoginButtonText,
                    onPressed: () => _signIn(),
                    isLoading: _isLoading,
                  )
                : AuthElevatedButton(
                    text: Strings.signupScreenSignupButtonText,
                    onPressed: () => _signUp(),
                    isLoading: _isLoading,
                  ),
          ),
          AuthTextFieldWrapper(
            FancyTextButton(
              isLoading: _isLoading,
              loadingIndicator: const SizedBox(),
              child: Text(
                _formType == AuthFormType.logIn
                    ? Strings.createAnAccountText
                    : Strings.authScreenLoginButtonText,
              ),
              onPressed: () {
                setState(
                  () {
                    if (_formType == AuthFormType.logIn) {
                      _formType = AuthFormType.signUp;
                    } else {
                      _formType = AuthFormType.logIn;
                    }
                    emailController.clear();
                    passwordController.clear();
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _signUp() async {
    _setLoading(true);
    await context.read<AuthService>().signUp(
          email: emailController.text,
          password: passwordController.text,
          context: context,
        );
    _setLoading(false);
  }

  Future<void> _signIn() async {
    _setLoading(true);
    await context.read<AuthService>().signIn(
          email: emailController.text,
          password: passwordController.text,
          context: context,
        );
    _setLoading(false);
  }

  _setLoading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }
}
