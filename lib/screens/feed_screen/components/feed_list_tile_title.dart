import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/models/models.dart';

class FeedListTileTitle extends StatelessWidget {
  const FeedListTileTitle({
    Key? key,
    required this.item,
  }) : super(key: key);
  final FeedItem item;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: item.authorName,
            style: AppStyles.listTileTitleStyle.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          TextSpan(
            text: ' ',
            style: AppStyles.listTileTitleStyle,
          ),
          TextSpan(
            text: item.feedItemType.asString(),
            style: AppStyles.listTileTitleStyle,
          ),
        ],
      ),
    );
  }
}
