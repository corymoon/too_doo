import 'package:flutter/material.dart';

import '/constants/constants.dart';

class UserDetailTodosListTile extends StatelessWidget {
  const UserDetailTodosListTile({
    Key? key,
    required this.color,
    required this.icon,
    required this.label,
    required this.number,
  }) : super(key: key);
  final Color color;
  final String label;
  final int number;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        color: color,
      ),
      title: Text(
        number.toString(),
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        label,
        style: TextStyle(
          color: AppColors.textColor70,
        ),
      ),
    );
  }
}
