import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/models/models.dart';

class FeedListTileSubtitle extends StatelessWidget {
  const FeedListTileSubtitle({
    Key? key,
    required this.item,
  }) : super(key: key);
  final FeedItem item;

  @override
  Widget build(BuildContext context) {
    return Text(
      item.displayDate,
      style: AppStyles.listTileSubtitleStyle,
    );
  }
}
