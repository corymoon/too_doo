import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/functions/functions.dart';
import '/models/models.dart';
import '/services/services.dart';
import 'feed_screen_components.dart';

class FeedListViewBuilder extends StatelessWidget {
  const FeedListViewBuilder({
    Key? key,
    required this.database,
    required this.items,
  }) : super(key: key);

  final List<FeedItem> items;
  final DatabaseService database;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      padding: AppPadding.listViewPassing,
      itemCount: items.length,
      itemBuilder: (_, index) {
        final item = items[index];
        return FeedListTile(
          database: database,
          item: item,
          onLongPress: () => _presentConfirmDeleteDialog(
            context,
            item.uid,
          ),
        );
      },
      separatorBuilder: (_, __) => AppWidgets.divider,
    );
  }

  Future<void> _presentConfirmDeleteDialog(BuildContext context, String itemId) async {
    var shouldDelete = await showConfirmationDialog(
      context,
      message: 'do you really want to delete this item from the feed?',
    );

    if (shouldDelete) {
      await database.deleteFeedItem(itemId);
    }
  }
}
