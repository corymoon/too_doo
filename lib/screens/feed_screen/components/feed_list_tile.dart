import 'package:flutter/material.dart';

import '/models/models.dart';
import '/services/database_service.dart';
import 'feed_screen_components.dart';

class FeedListTile extends StatelessWidget {
  const FeedListTile({
    Key? key,
    required this.database,
    required this.item,
    required this.onLongPress,
  }) : super(key: key);
  final FeedItem item;
  final DatabaseService database;
  final VoidCallback onLongPress;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: item.authorId == database.uid ? onLongPress : null,
      leading: FeedListTileAvatar(item: item),
      title: FeedListTileTitle(item: item),
      subtitle: FeedListTileSubtitle(item: item),
    );
  }
}
