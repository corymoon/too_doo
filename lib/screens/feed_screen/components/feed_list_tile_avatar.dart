import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
// import '/constants/constants.dart';
import '/models/models.dart';
import '/widgets/widgets.dart';

class FeedListTileAvatar extends StatelessWidget {
  const FeedListTileAvatar({
    Key? key,
    required this.item,
  }) : super(key: key);
  final FeedItem item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: Badge(
        animationDuration: Duration.zero,
        position: BadgePosition.bottomEnd(),
        padding: const EdgeInsets.all(3.0),
        badgeContent: Icon(
          item.icon,
          size: 18.0,
          // color: AppColors.textColor,
          color: item.onColor,
        ),
        badgeColor: item.color,
        child: UserAvatar(
          radius: 25.0,
          name: item.authorName,
          photoURL: item.authorPhotoURL,
        ),
      ),
    );
  }
}
