import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '/models/models.dart';
import '/screens/screens.dart';
import '/services/database_service.dart';
// import '/widgets/widgets.dart';
import 'components/feed_screen_components.dart';

class FeedScreen extends StatelessWidget {
  const FeedScreen(this.user, {Key? key}) : super(key: key);
  final User? user;

  @override
  Widget build(BuildContext context) {
    final database = DatabaseService(user!.uid);
    return Scaffold(
      body: StreamBuilder<List<FeedItem>>(
        initialData: const [],
        stream: database.streamFeedItems(),
        builder: (context, snapshot) {
          final items = snapshot.data;
          // final numberOfUsers = users!.length;

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LoadingScreen();
          }

          if (items!.isEmpty) {
            return const NoContentScreen();
          }

          return FeedListViewBuilder(database: database, items: items);
        },
      ),
    );
  }
}
