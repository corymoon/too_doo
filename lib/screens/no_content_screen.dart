import 'package:flutter/material.dart';
import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:blobs/blobs.dart' as blob;

import '/constants/colors.dart';
import '/themes/themes.dart';

class NoContentScreen extends StatelessWidget {
  const NoContentScreen({
    Key? key,
    this.text = 'nothing to do...\nadd something',
  }) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: blob.Blob.animatedRandom(
        duration: const Duration(seconds: 3),
        edgesCount: 15,
        loop: true,
        styles: blob.BlobStyles(
          color: AppColors.primaryColor.withOpacity(0.5),
        ),
        size: context.mediaQuery.size.width,
        child: Center(
          child: Text(
            text,
            style: kAppTextTheme.headline5!.copyWith(
              color: AppColors.textColor70,
            ),
          ),
        ),
      ),
    );
  }
}
