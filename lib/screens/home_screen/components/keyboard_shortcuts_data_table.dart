import 'package:flutter/material.dart';

import '/models/models.dart';

class KeyboardShortcutsDataTable extends StatelessWidget {
  const KeyboardShortcutsDataTable({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const [
        DataColumn(label: Text('action')),
        DataColumn(label: Text('key')),
      ],
      rows: kAppShortcutKeys
          .map(
            (e) => DataRow(
              cells: [
                DataCell(Text(e.action)),
                DataCell(Text(e.key)),
              ],
            ),
          )
          .toList(),
    );
  }
}
