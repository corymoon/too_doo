import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';

import '/models/models.dart';

class ShortcutKeyListTile extends StatelessWidget {
  const ShortcutKeyListTile(this.item, {Key? key}) : super(key: key);
  final ShortcutKey item;

  @override
  Widget build(BuildContext context) {
    final _screenWidth = context.mediaQuery.size.width;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          child: Text(item.action),
          width: _screenWidth * 0.4,
        ),
        SizedBox(
          child: Text(item.key),
          width: _screenWidth * 0.2,
        ),
      ],
    );
    // return Padding(
    //   padding: const EdgeInsets.symmetric(horizontal: 8.0),
    //   child: ListTile(
    //     trailing: Text(
    //       item.key,
    //       style: const TextStyle(
    //         fontWeight: FontWeight.bold,
    //       ),
    //     ),
    //     title: Text(item.action),
    //   ),
    // );
  }
}
