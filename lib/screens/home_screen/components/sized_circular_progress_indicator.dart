import 'package:flutter/material.dart';

import '/constants/constants.dart';

class SizedCircularProgressIndicator extends StatelessWidget {
  const SizedCircularProgressIndicator({
    Key? key,
    this.radius = 18.0,
    this.color,
  }) : super(key: key);
  final double radius;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: radius * 2,
      width: radius * 2,
      child: Center(
        child: CircularProgressIndicator(
          color: color ?? AppColors.primaryColor,
        ),
      ),
    );
  }
}
