import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '/constants/constants.dart';
import '/functions/functions.dart';
import '/screens/screens.dart';
import '/screens/todos_screen/components/todos_screen_components.dart';
import '/services/services.dart';
import '/widgets/widgets.dart';
import 'components/home_screen_components.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen(this.user, {Key? key}) : super(key: key);
  final User? user;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  final titleController = TextEditingController();

  @override
  void dispose() {
    titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final database = DatabaseService(widget.user!.uid);
    final shortcutsService = context.read<ShortcutsService>();

    final _screens = [
      TodosScreen(widget.user),
      ArchiveScreen(widget.user),
      FeedScreen(widget.user),
      AccountScreen(widget.user),
    ];
    var _sidebarWidth = 250.0;

    return RawKeyboardListener(
      autofocus: true,
      focusNode: FocusNode(),
      onKey: (event) {
        if (kIsWeb && !shortcutsService.dialogIsShowing) {
          if (event is RawKeyDownEvent) {
            // add new todo by pressing N
            if (event.isKeyPressed(LogicalKeyboardKey.keyN)) {
              _showAddTodoDialog(database);
            }

            // navigate to todos by pressing T
            if (event.isKeyPressed(LogicalKeyboardKey.keyT)) {
              _navigateToIndex(0);
            }

            // navigate to archive by pressing A
            if (event.isKeyPressed(LogicalKeyboardKey.keyA)) {
              _navigateToIndex(1);
            }

            // navigate to feed by pressing F
            if (event.isKeyPressed(LogicalKeyboardKey.keyF)) {
              _navigateToIndex(2);
            }

            // navigate to profile by pressing P
            if (event.isKeyPressed(LogicalKeyboardKey.keyP)) {
              _navigateToIndex(3);
            }
          }
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: _buildAppBarTitle(_selectedIndex),
          actions: [
            if (_selectedIndex == 1)
              IconButton(
                icon: const Icon(AppIcons.helpIcon),
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) => SimpleDialog(
                      contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
                      title: const Text('archived items lifecycle'),
                      children: [
                        SizedBox(
                          width: context.mediaQuery.size.width * 0.5,
                          child: const Text(
                            'all archived items will be deleted on the first day of the month, unless marked saved.',
                            style: TextStyle(
                              height: 1.5,
                            ),
                          ),
                        ),
                        Space.verticalMedium,
                      ],
                    ),
                  );
                },
              ),
            _buildAppBarMenu()
          ],
        ),
        body: Row(
          children: [
            if (isLargeScreen(context)) _buildSidebar(_sidebarWidth, database, shortcutsService),
            Expanded(child: _screens.elementAt(_selectedIndex)),
          ],
        ),
        bottomNavigationBar: isLargeScreen(context) ? null : _buildNavigationBar(),
      ),
    );
  }

  Widget _buildSidebar(
    double sidebarWidth,
    DatabaseService database,
    ShortcutsService shortcutsService,
  ) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.elevatedSurfaceBackgroundColor,
        borderRadius: AppShapes.rightCornersBigRadius,
      ),
      width: sidebarWidth,
      child: ListView(
        padding: const EdgeInsets.all(20.0),
        children: [
          _buildDesktopAddButton(database),
          ...kAppMenuItems
              .map(
                (item) => ListTile(
                  selected: _selectedIndex == item.index,
                  title: Text(item.label),
                  leading: Icon(_selectedIndex == item.index ? item.activeIcon : item.inactiveIcon),
                  onTap: () => _navigateToIndex(item.index),
                ),
              )
              .toList(),
        ],
      ),
    );
  }

  Widget _buildDesktopAddButton(DatabaseService database) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0, left: 12.0, right: 12.0),
      child: GradientElevatedButton(
        label: 'add',
        icon: AppIcons.fabAddIcon,
        onPressed: () => _showAddTodoDialog(database),
      ),
    );
  }

  Future<void> _showAddTodoDialog(DatabaseService database) async {
    final ShortcutsService shortcutsService = context.read<ShortcutsService>();
    shortcutsService.disableShortcuts();
    await showDialog(
      context: context,
      builder: (context) => EnterTodoDialog(
        title: Strings.newTodoTitle,
        controller: titleController,
        onSubmit: () => createNewTodo(context, database, titleController).then((_) {
          if (_selectedIndex != 0) {
            _navigateToIndex(0);
          }
        }),
        submitText: Strings.newTodoButtonText,
      ),
    );
    shortcutsService.enableShortcuts();
  }

  Widget _buildNavigationBar() {
    return NavigationBar(
      selectedIndex: _selectedIndex,
      onDestinationSelected: (index) => _navigateToIndex(index),
      destinations: kAppMenuItems
          .map(
            (item) => NavigationDestination(
              icon: Icon(
                _selectedIndex == item.index ? item.activeIcon : item.inactiveIcon,
                color: _selectedIndex == item.index
                    ? Color.alphaBlend(Dracula.purple400, AppColors.textColor)
                    : AppColors.textColor70,
              ),
              label: item.label,
            ),
          )
          .toList(),
    );
  }

  Widget _buildAppBarTitle(int index) {
    String title;
    IconData icon;

    switch (index) {
      case 0:
        title = Strings.appTitle;
        icon = AppIcons.appLogoIcon;
        break;
      case 1:
        title = Strings.archiveScreenTitle;
        icon = AppIcons.todoArchiveStateIcon;
        break;
      case 2:
        title = Strings.feedScreenTitle;
        icon = AppIcons.feedIcon.active;
        break;
      case 3:
        title = Strings.profileScreenTitle;
        icon = AppIcons.profileIcon.active;
        break;
      default:
        title = Strings.appTitle;
        icon = AppIcons.appLogoIcon;
        break;
    }

    return AppBarIconTitle(
      title: title,
      icon: icon,
    );
  }

  Widget _buildAppBarMenu() {
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: PopupMenuButton(
        shape: AppShapes.allCornersMediumRadius,
        color: AppColors.elevatedSurfaceBackgroundColor,
        icon: const Icon(AppIcons.appBarMenuIcon),
        itemBuilder: (context) {
          return [
            if (kIsWeb && isLargeScreen(context))
              PopupMenuItem(
                child: ListTile(
                  leading: const Icon(AppIcons.keyboardShortcutsIcon),
                  title: const Text(Strings.keyboardShortcutsText),
                  onTap: () => _showKeyboardShortcutsDialog(),
                ),
              ),
            PopupMenuItem(
              child: ListTile(
                leading: Icon(AppIcons.profileIcon.active),
                title: const Text(Strings.profileScreenTitle),
                onTap: () {
                  context.navigator.pop();
                  _navigateToIndex(3);
                },
              ),
            ),
            PopupMenuItem(
              child: ListTile(
                leading: const Icon(AppIcons.logoutIcon),
                title: const Text(Strings.logOutText),
                onTap: () => _confirmSignOut(),
              ),
            ),
          ];
        },
      ),
    );
  }

  void _navigateToIndex(int index) {
    if (_selectedIndex != index) {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  Future<void> _showKeyboardShortcutsDialog() async {
    final ShortcutsService shortcutsService = context.read<ShortcutsService>();
    context.navigator.pop();
    shortcutsService.disableShortcuts();
    await showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('keyboard shortcuts'),
            IconButton(
              icon: const Icon(
                AppIcons.closeIcon,
                size: 16.0,
              ),
              onPressed: () => context.navigator.pop(),
            ),
          ],
        ),
        children: const [KeyboardShortcutsDataTable()],
      ),
    );
    shortcutsService.enableShortcuts();
  }

  Future<void> _confirmSignOut() async {
    final shouldSignOut = await showConfirmationDialog(
      context,
      message: 'are you sure you want to log out?',
    );
    if (shouldSignOut) {
      await context.read<AuthService>().signOut(context);
    } else {
      context.navigator.pop();
    }
  }
}
