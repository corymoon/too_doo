import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/extensions/extensions.dart';
import '/functions/functions.dart';
import '/models/models.dart';

class TodoTitle extends StatelessWidget {
  const TodoTitle({Key? key, required this.todo}) : super(key: key);
  final Todo todo;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildIcon(),
        if (todo.isPriority || todo.hasDueDate) Space.horizontalSmall,
        Expanded(
          child: Text(
            todo.title,
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.visible,
            style: AppStyles.listTileTitleStyle.copyWith(
              color: todo.titleColor,
              decoration: todo.isComplete ? TextDecoration.lineThrough : null,
              decorationColor: AppColors.textColor50,
              decorationThickness: 2.0,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildIcon() {
    IconData? icon;
    Color? color;

    if (todo.hasDueDate && todo.isPriority) {
      icon = AppIcons.todoPriorityWithDueDateIcon;
      color = todo.titleColor;
    }
    if (todo.hasDueDate && !todo.isPriority) {
      icon = AppIcons.todoWithDueDateIcon;
      color = todo.titleColor;
    }
    if (todo.isPriority && !todo.hasDueDate) {
      icon = AppIcons.todoPriorityStateIcon;
      color = AppColors.priorityColor;
    }

    if (icon == null) {
      return const SizedBox();
    } else {
      return Icon(
        icon,
        size: 18.0,
        color: color!,
      );
    }
  }
}
