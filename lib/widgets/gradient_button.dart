import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';
import '/constants/constants.dart';

class GradientElevatedButton extends StatelessWidget {
  const GradientElevatedButton({
    Key? key,
    required this.label,
    required this.onPressed,
    this.icon,
    this.gradient,
    this.isLoading = false,
    this.style,
  }) : super(key: key);
  final VoidCallback onPressed;
  final String label;
  final IconData? icon;
  final LinearGradient? gradient;
  final bool isLoading;
  final ButtonStyle? style;

  @override
  Widget build(BuildContext context) {
    return FancyElevatedButton(
      isLoading: isLoading,
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: AppShapes.allCornersLargeRadius,
        padding: const EdgeInsets.all(0.0),
      ),
      child: Ink(
        decoration: BoxDecoration(
          gradient: gradient ?? AppColors.primaryGradient,
          borderRadius: BorderRadius.circular(Numbers.borderRadiusLarge),
        ),
        child: Container(
          constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0),
          alignment: Alignment.center,
          child: icon == null
              ? Text(
                  label,
                  textAlign: TextAlign.center,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(icon),
                    Space.horizontalSmall,
                    Text(
                      label,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
