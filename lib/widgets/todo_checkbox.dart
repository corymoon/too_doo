import 'package:circle_checkbox/redev_checkbox.dart';
import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/extensions.dart';
import '/functions/functions.dart';
import '/models/models.dart';
import '/services/services.dart';

class TodoCheckbox extends StatelessWidget {
  const TodoCheckbox({
    Key? key,
    required this.todo,
    required this.database,
  }) : super(key: key);
  final Todo todo;
  final DatabaseService database;

  @override
  Widget build(BuildContext context) {
    return CircleCheckbox(
      checkColor: AppColors.textColor,
      onChanged: todo.isSnoozed
          ? (value) {}
          : todo.isArchived
              ? (value) {}
              : (value) => _toggleCheckbox(),
      fillColor: _fillColor(),
      value: todo.isComplete,
    );
  }

  Future<void> _toggleCheckbox() async {
    if (todo.isComplete) {
      await database.incompleteTodo(todo);
    } else {
      await database.completeTodo(todo);
    }
  }

  MaterialStateColor _archiveFill() {
    return materialStateColor(
      active: AppColors.textColor50,
      inactive: AppColors.textColor50,
    );
  }

  MaterialStateColor _snoozeFill() {
    return materialStateColor(
      active: AppColors.snoozedTextColor,
      inactive: AppColors.snoozedTextColor,
    );
  }

  MaterialStateProperty<Color?>? _fillColor() {
    if (todo.state == TodoState.archived) {
      return _archiveFill();
    }
    if (todo.state == TodoState.snoozed) {
      return _snoozeFill();
    }
    return null;
  }
}
