import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart' as md;
import 'package:provider/provider.dart';

import '/constants/constants.dart';
import '/models/models.dart';
import '/services/services.dart';

class TodoViewNotesButton extends StatelessWidget {
  const TodoViewNotesButton({Key? key, required this.todo}) : super(key: key);
  final Todo todo;

  @override
  Widget build(BuildContext context) {
    final ShortcutsService shortcutsService = context.read<ShortcutsService>();
    return IconButton(
      tooltip: 'view notes',
      icon: const Icon(AppIcons.notesIcon, size: 20.0),
      onPressed: () async {
        shortcutsService.disableShortcuts();
        await showModalBottomSheet(
          context: context,
          builder: (context) => BottomSheet(
            onClosing: () {},
            builder: (context) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 24.0),
              child: md.MarkdownBody(data: todo.notes!),
            ),
          ),
        );
        shortcutsService.enableShortcuts();
      },
    );
  }
}
