import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/themes/themes.dart';
import '/widgets/widgets.dart';

class AppBarIconTitle extends StatelessWidget {
  const AppBarIconTitle({
    Key? key,
    required this.icon,
    required this.title,
  }) : super(key: key);
  final IconData icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          // color: AppColors.textColor,
          color: AppColors.primaryColor,
        ),
        Space.horizontalSmall,
        GradientText(
          title,
          gradient: AppColors.primaryGradient,
          style: kAppTextTheme.headline6!.copyWith(color: AppColors.textColor),
        ),
        // Text(
        //   title,
        //   style: kAppTextTheme.headline6!.copyWith(color: AppColors.textColor),
        // ),
      ],
    );
  }
}
