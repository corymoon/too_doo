import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '/constants/constants.dart';
import '/extensions/extensions.dart';

class UserAvatar extends StatelessWidget {
  const UserAvatar({
    Key? key,
    required this.name,
    required this.photoURL,
    this.radius,
  }) : super(key: key);
  final String name;
  final String photoURL;
  final double? radius;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundColor: AppColors.primaryColor,
      child: photoURL.isNotEmpty ? _buildPhoto() : _buildInitials(),
    );
  }

  /// build photo widget
  Widget _buildPhoto() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius!),
      child: CachedNetworkImage(
        progressIndicatorBuilder: (_, __, ___) => Center(
          child: SizedBox(
            width: radius,
            height: radius,
            child: const CircularProgressIndicator(
              color: AppColors.onPrimary,
            ),
          ),
        ),
        errorWidget: (_, __, ___) => const Icon(
          AppIcons.defaultProfilePhotoIcon,
          color: AppColors.onPrimary,
        ),
        imageUrl: photoURL,
        fit: BoxFit.cover,
        height: radius! * 2,
        width: radius! * 2,
      ),
    );
  }

  /// build initials widget
  Widget _buildInitials() {
    return Text(
      name.initials(),
      style: const TextStyle(
        color: AppColors.onPrimary,
      ),
    );
  }
}
