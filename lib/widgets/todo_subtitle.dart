import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/extensions/extensions.dart';
import '/models/models.dart';

class TodoSubtitle extends StatelessWidget {
  const TodoSubtitle({Key? key, required this.todo}) : super(key: key);
  final Todo todo;

  // with due date       => no icon    | 'due in' text
  // snoozed             => moon icon  | created date
  // complete            => check icon | completed date
  // other               => no icon    | created date

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (todo.isSnoozed || todo.isComplete) _buildPrefixIcon(),
        if (todo.isSnoozed || todo.isComplete) Space.horizontalSmall,
        Text(
          _makeSubtitleText(),
          style: AppStyles.listTileSubtitleStyle.copyWith(
            color: todo.subtitleColor,
          ),
        ),
      ],
    );
  }

  String _makeSubtitleText() {
    if (todo.hasDueDate) {
      return todo.dueIn;
    } else if (todo.isComplete) {
      return _makeDuration();
    } else {
      return todo.displayDate;
    }
  }

  String _makeDuration() {
    String duration;
    if (todo.duration != null) {
      duration = todo.duration!;
    } else {
      duration = todo.displayDate;
    }
    return duration;
  }

  Widget _buildPrefixIcon() {
    IconData icon = AppIcons.todoPriorityStateIcon;
    Color color = AppColors.textColor50;

    if (todo.hasDueDate) {
      color = AppColors.errorColor.withOpacity(0.85);
    }

    if (todo.isSnoozed) {
      icon = AppIcons.todoSnoozedStateIcon;
      color = AppColors.snoozedTextColor;
    }

    if (todo.isComplete) {
      icon = AppIcons.todoCompleteStateIcon;
    }

    return Icon(
      icon != AppIcons.todoPriorityStateIcon ? icon : null,
      color: color,
      size: 15.0,
    );
  }
}
