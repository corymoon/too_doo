import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/extensions.dart';

class ListTileMenuItem extends StatelessWidget {
  const ListTileMenuItem({
    Key? key,
    required this.activeString,
    required this.inactiveString,
    required this.state,
    required this.todoState,
  }) : super(key: key);

  final TodoState state;
  final bool todoState;
  final String activeString;
  final String inactiveString;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(todoState ? state.inactiveIcon : state.activeIcon),
        Space.horizontalSmall,
        Text(todoState ? activeString : inactiveString),
      ],
    );
  }
}
