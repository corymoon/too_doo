class Paths {
  static const String _userPath = 'users';
  static const String _todoPath = 'todos';

  static String todo(uid, todoId) => '$_userPath/$uid/$_todoPath/$todoId';
  static String todos(uid) => '$_userPath/$uid/$_todoPath';

  static String user(uid) => '$_userPath/$uid';
  static String users() => _userPath;
}
