import 'package:cloud_firestore/cloud_firestore.dart';

import '/enums/enums.dart';
import '/extensions/extensions.dart';
import '/models/models.dart';
import 'paths.dart';

class DatabaseService {
  final String uid;
  DatabaseService(this.uid);

  /// firebase instance
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  /// method to create a new todo
  Future createNewTodo(String title) async {
    return await _firestore.collection(Paths.todos(uid)).add({
      'title': title,
      'state': 'active',
      'created': DateTime.now(),
      'completedAt': DateTime(1970, 1, 1),
      'dueDate': DateTime(1970, 1, 1),
      // 'notes': '',
    });
  }

  /// generic method to update a todo
  Future<void> updateTodo({
    required Todo todo,
    String? title,
    DateTime? created,
    DateTime? completedAt,
    TodoState? state,
    String? notes,
    DateTime? dueDate,
    bool? saved,
  }) async {
    Todo newTodo = todo.copyWith(
      completedAt: completedAt ?? todo.completedAt,
      created: created ?? todo.created,
      title: title ?? todo.title,
      state: state ?? todo.state,
      notes: notes ?? todo.notes,
      dueDate: dueDate ?? todo.dueDate,
      saved: saved ?? todo.saved,
    );
    await _firestore.doc(Paths.todo(uid, todo.uid)).update(newTodo.toMap());
    await _updateUserLastActivity();
  }

  /// method to remove a todo by it's uid
  Future<void> removeTodo(Todo todo) async {
    await _firestore.doc(Paths.todo(uid, todo.uid)).delete();
    await _updateUserLastActivity();
  }

  /// generic method to update the current user's last activity time
  Future<void> _updateUserLastActivity() async {
    await _firestore.doc(Paths.user(uid)).update({'lastActivity': DateTime.now()});
  }

  /// generic method to toggle the state of a todo
  Future<void> _toggleTodo(Todo todo, DateTime date) async => await updateTodo(
        todo: todo,
        completedAt: date,
        state: todo.isComplete ? TodoState.active : TodoState.complete,
      );

  /// method mark a todo as complete
  Future completeTodo(Todo todo) async {
    await _toggleTodo(todo, DateTime.now());
  }

  /// method to mark a todo as incomplete
  Future incompleteTodo(Todo todo) async {
    await _toggleTodo(todo, DateTime(1970, 1, 1));
  }

  /// method to mark a todo as archived
  Future<void> archiveTodo(Todo todo) async {
    await updateTodo(todo: todo, state: TodoState.archived);
  }

  /// method to remove a todo from the archive
  Future<void> unarchiveTodo(Todo todo) async {
    await updateTodo(
      todo: todo,
      state: todo.isComplete ? TodoState.complete : TodoState.active,
    );
  }

  /// method to mark a todo as snoozed
  Future<void> snoozeTodo(Todo todo) async {
    await updateTodo(todo: todo, state: TodoState.snoozed);
  }

  /// method to remove a todo from the archive
  Future<void> wakeTodo(Todo todo) async {
    await updateTodo(
      todo: todo,
      state: todo.isComplete ? TodoState.complete : TodoState.active,
    );
  }

  /// method to convert a list of maps to a list of todos from firestore
  List<Todo> todoFromFirestore(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      return Todo.fromFirestore(data, doc.id);
    }).toList();
  }

  /// method to convert a list of maps to a list of todos from firestore
  List<FeedItem> feedItemsFromFirestore(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      return FeedItem.fromFirestore(data, doc.id);
    }).toList();
  }

  /// method to convert a list of maps to a list of appUsers from firestore
  List<AppUser> usersListFromFirestore(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      return AppUser.fromFirestore(data);
    }).toList();
  }

  /// method to convert a map to an appuser from firestore
  AppUser userFromFirestore(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
    return AppUser.fromFirestore(data);
  }

  /// generic method to stream a list of todos from firestore based on their archive status
  Stream<List<FeedItem>> streamFeedItems() {
    return _firestore
        .collection('feed')
        .orderBy('date', descending: true)
        .limit(100)
        .snapshots()
        .map(feedItemsFromFirestore);
  }

  /// method to stream a list of todos from firestore
  Stream<List<Todo>> listTodos() =>
      _firestore.collection(Paths.todos(uid)).orderBy('state').snapshots().map(todoFromFirestore);

  /// method to list
  Stream<List<Todo>> listArchivedTodos() =>
      _firestore.collection(Paths.todos(uid)).snapshots().map(todoFromFirestore);

  Future<AppUser> getUserDocFuture() async {
    final doc = await _firestore.doc(Paths.user(uid)).get();
    return userFromFirestore(doc);
  }

  /// method to get the current user's firestore document
  Stream<AppUser> getUserDoc() =>
      _firestore.doc(Paths.user(uid)).snapshots().map(userFromFirestore);

  /// method to update the current user's firestore document
  Future<void> updateUserDoc(AppUser appUser) async {
    var updatedUser = appUser.copyWith(
      displayName: appUser.displayName,
      email: appUser.email,
      photoURL: appUser.photoURL,
      uid: appUser.uid,
      activeTodos: appUser.activeTodos,
      archivedTodos: appUser.archivedTodos,
      completeTodos: appUser.completeTodos,
      snoozedTodos: appUser.snoozedTodos,
      lastActivity: appUser.lastActivity,
    );

    await _firestore.doc(Paths.user(uid)).set(
          updatedUser.toMap(),
          SetOptions(merge: true),
        );
  }

  ///
  Stream<List<AppUser>> getUsers() => _firestore
      .collection(Paths.users())
      .orderBy('lastActivity', descending: true)
      .limit(50)
      .snapshots()
      .map(usersListFromFirestore);

  Future<void> deleteFeedItem(String itemId) async {
    await _firestore.collection('feed').doc(itemId).delete();
  }

  Future<void> toggleSavedTodo(Todo todo) async {
    await updateTodo(
      todo: todo,
      saved: !todo.saved!,
    );
  }
}
