import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '/functions/functions.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth;
  AuthService(this._firebaseAuth);

  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<void> _authMethodWithError({
    required Future authMethod,
    required BuildContext context,
  }) async {
    try {
      await authMethod;
    } on FirebaseAuthException catch (e) {
      showSnackbarError(context, text: e.message!);
    } on FirebaseException catch (e) {
      showSnackbarError(context, text: e.message!);
    }
  }

  Future<void> signIn({
    required String email,
    required String password,
    required BuildContext context,
  }) async {
    _authMethodWithError(
      context: context,
      authMethod: _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      ),
    );
  }

  Future<void> signUp({
    required String email,
    required String password,
    required BuildContext context,
  }) async {
    await _authMethodWithError(
      context: context,
      authMethod: _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      ),
    );
  }

  Future<void> signOut(BuildContext context) async {
    _authMethodWithError(authMethod: _firebaseAuth.signOut(), context: context);
  }

  Future<void> sendVerificationEmail(BuildContext context) async {
    _authMethodWithError(
      context: context,
      authMethod: _firebaseAuth.currentUser!.sendEmailVerification(),
    );
  }

  Future<void> sendPasswordReset(BuildContext context) async {
    _authMethodWithError(
      context: context,
      authMethod: _firebaseAuth.sendPasswordResetEmail(
        email: _firebaseAuth.currentUser!.email!,
      ),
    );
  }
}
