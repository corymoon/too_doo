class ShortcutsService {
  bool _dialogIsShowing = false;

  bool get dialogIsShowing => _dialogIsShowing;

  void setDialogStatus(bool newStatus) {
    _dialogIsShowing = newStatus;
  }

  void enableShortcuts() {
    // print('*** enabling shortcut keys...');
    _dialogIsShowing = false;
  }

  void disableShortcuts() {
    // print('*** disabling shortcut keys...');
    _dialogIsShowing = true;
  }
}
