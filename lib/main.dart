import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'constants/constants.dart';
import 'firebase_options.dart';
import 'screens/screens.dart';
import 'services/services.dart';
import 'themes/themes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_) => AuthService(FirebaseAuth.instance),
        ),
        Provider<ShortcutsService>(
          create: (_) => ShortcutsService(),
        ),
        StreamProvider<User?>(
          initialData: null,
          create: (context) => context.read<AuthService>().authStateChanges,
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: Strings.appTitle,
        theme: ThemeData(
          colorScheme: kAppColorScheme,
          checkboxTheme: kAppCheckboxTheme,
          errorColor: AppColors.errorColor,
          scaffoldBackgroundColor: AppColors.backgroundColor,
          primarySwatch: AppColors.primaryColor,
          appBarTheme: kAppAppBarTheme,
          textTheme: kAppTextTheme,
          dialogTheme: kAppDialogTheme,
          inputDecorationTheme: kAppInputDecorationTheme,
          elevatedButtonTheme: kAppElevatedButtonTheme,
          buttonTheme: kAppButtonTheme,
          unselectedWidgetColor: AppColors.textColor50,
          timePickerTheme: kAppTimePickerTheme,
          bottomSheetTheme: kAppBottomSheetTheme,
          navigationBarTheme: kAppNavigationBarTheme,
        ),
        home: const AuthWrapper(),
      ),
    );
  }
}

class AuthWrapper extends StatelessWidget {
  const AuthWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final User? user = context.watch<User?>();

    if (user != null) {
      // return TodosScreen(user);
      return HomeScreen(user);
    }
    return const LoginScreen();
  }
}
