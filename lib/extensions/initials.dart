extension Initials on String {
  String initials() {
    String result = '';
    List<String> words = split(' ');
    for (var element in words) {
      if (element.isNotEmpty && result.length < 2) {
        result += element[0];
      }
    }

    return result.trim().toUpperCase();
  }
}
