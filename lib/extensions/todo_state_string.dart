import '/enums/enums.dart';

extension TodoStateString on TodoStates {
  String asString() {
    switch (this) {
      case TodoStates.active:
        return 'activeTodos';
      case TodoStates.archived:
        return 'archivedTodos';
      case TodoStates.complete:
        return 'completeTodos';
      case TodoStates.snoozed:
        return 'snoozedTodos';
      default:
        return '';
    }
  }
}
