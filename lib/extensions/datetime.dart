import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

extension MonthDayYear on DateTime {
  String get monthDayYear => DateFormat.yMd().format(this);
}

extension HourMinuteAmpm on TimeOfDay {
  String get hourMinuteAmpm {
    return DateFormat.jm().format(DateTime(1970, 1, 1, hour, minute));
  }
}

extension IsSameDay on DateTime {
  bool isSameDay(DateTime other) {
    final sameYear = year == other.year;
    final sameMonth = month == other.month;
    final sameDay = day == other.day;
    return (sameYear && sameMonth && sameDay);
  }
}
