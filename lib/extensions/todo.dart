import 'package:duration/duration.dart';
import 'package:flutter/material.dart';

import '/constants/constants.dart';
import '/enums/enums.dart';
import '/extensions/datetime.dart';
import '/models/models.dart';

extension TitleColor on Todo {
  Color get titleColor {
    if (hasDueDate) {
      final today = DateTime.now();
      final nineDaysBefore = dueDate.subtract(const Duration(days: 9));
      final eightDaysBefore = dueDate.subtract(const Duration(days: 8));
      final sevenDaysBefore = dueDate.subtract(const Duration(days: 7));
      final sixDaysBefore = dueDate.subtract(const Duration(days: 6));
      final fiveDaysBefore = dueDate.subtract(const Duration(days: 5));
      final fourDaysBefore = dueDate.subtract(const Duration(days: 4));
      final threeDaysBefore = dueDate.subtract(const Duration(days: 3));
      final twoDaysBefore = dueDate.subtract(const Duration(days: 2));
      final oneDayBefore = dueDate.subtract(const Duration(days: 1));

      if (today.isSameDay(nineDaysBefore)) {
        return Color.alphaBlend(Dracula.red100, AppColors.textColor);
      } else if (today.isSameDay(eightDaysBefore)) {
        return Color.alphaBlend(Dracula.red200, AppColors.textColor);
      } else if (today.isSameDay(sevenDaysBefore)) {
        return Color.alphaBlend(Dracula.red300, AppColors.textColor);
      } else if (today.isSameDay(sixDaysBefore)) {
        return Color.alphaBlend(Dracula.red400, AppColors.textColor);
      } else if (today.isSameDay(fiveDaysBefore)) {
        return Color.alphaBlend(Dracula.red500, AppColors.textColor);
      } else if (today.isSameDay(fourDaysBefore)) {
        return Color.alphaBlend(Dracula.red600, AppColors.textColor);
      } else if (today.isSameDay(threeDaysBefore)) {
        return Color.alphaBlend(Dracula.red700, AppColors.textColor);
      } else if (today.isSameDay(twoDaysBefore)) {
        return Color.alphaBlend(Dracula.red800, AppColors.textColor);
      } else if (today.isSameDay(oneDayBefore)) {
        return Color.alphaBlend(Dracula.red900, AppColors.textColor);
      } else if (today.isSameDay(dueDate)) {
        return Color.alphaBlend(Dracula.red, AppColors.textColor);
      } else {
        return AppColors.textColor;
      }
    }

    if (isPriority) {
      return AppColors.priorityColor;
    }

    if (isComplete) {
      return AppColors.textColor50;
    }

    if (isSnoozed) {
      return AppColors.snoozedTextColor;
    }

    return AppColors.textColor;
  }
}

extension SubtitleColor on Todo {
  Color get subtitleColor {
    Color theColor;
    if (hasDueDate) {
      theColor = titleColor;
    }
    if (isSnoozed) {
      theColor = AppColors.snoozedTextColor;
    } else {
      theColor = AppColors.textColor40;
    }
    return theColor.withOpacity(0.5);
  }
}

extension DueIn on Todo {
  String get dueIn {
    String timeTaken;
    if (DateTime.now().isBefore(dueDate)) {
      timeTaken = printDuration(
        dueDate.difference(DateTime.now()),
        tersity: DurationTersity.minute,
      );
      return 'in $timeTaken';
    } else {
      timeTaken = printDuration(
        DateTime.now().difference(dueDate),
        tersity: DurationTersity.minute,
      );
      return '$timeTaken ago';
    }
  }
}

extension BoolGetters on Todo {
  bool get isActive => state == TodoState.active;
  bool get isComplete => state == TodoState.complete || completedAt != DateTime(1970, 1, 1);
  bool get isPriority => state == TodoState.priority;
  bool get isSnoozed => state == TodoState.snoozed;
  bool get isArchived => state == TodoState.archived;
  bool get hasDueDate => dueDate != DateTime(1970, 1, 1);
  bool get hasNotes => notes != null && notes!.isNotEmpty;
}
