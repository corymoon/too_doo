import 'package:flutter/material.dart';

import '/constants/constants.dart';

import '/enums/enums.dart';

extension TodoStateIcons on TodoState {
  IconData get activeIcon {
    switch (this) {
      case TodoState.archived:
        return AppIcons.todoArchiveStateIcon;
      case TodoState.complete:
        return AppIcons.todoCompleteStateIcon;
      case TodoState.priority:
        return AppIcons.todoPriorityStateIcon;
      case TodoState.snoozed:
        return AppIcons.todoSnoozedStateIcon;
      case TodoState.active:
      default:
        return AppIcons.todoDefaultStateIcon;
    }
  }

  IconData get inactiveIcon {
    switch (this) {
      case TodoState.archived:
        return AppIcons.todoUnarchiveStateIcon;
      case TodoState.complete:
        return AppIcons.todoIncompleteStateIcon;
      case TodoState.priority:
        return AppIcons.todoNotPriorityStateIcon;
      case TodoState.snoozed:
        return AppIcons.todoWakeStateIcon;
      case TodoState.active:
      default:
        return AppIcons.todoActiveStateIcon;
    }
  }
}

extension AsString on TodoState {
  String asString() {
    switch (this) {
      case TodoState.complete:
        return 'complete';
      case TodoState.priority:
        return 'priority';
      case TodoState.snoozed:
        return 'snoozed';
      case TodoState.archived:
        return 'archived';
      case TodoState.active:
      default:
        return 'active';
    }
  }
}

extension ToTodoState on String {
  TodoState toTodoState() {
    switch (this) {
      case 'complete':
        return TodoState.complete;
      case 'priority':
        return TodoState.priority;
      case 'snoozed':
        return TodoState.snoozed;
      case 'archived':
        return TodoState.archived;
      case 'active':
      default:
        return TodoState.active;
    }
  }
}
