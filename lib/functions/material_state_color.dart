import 'package:flutter/material.dart';

MaterialStateColor materialStateColor({
  required Color active,
  required Color inactive,
}) {
  return MaterialStateColor.resolveWith(
    (states) => states.contains(MaterialState.selected) ? active : inactive,
  );
}
