import 'package:flutter/material.dart';
import 'package:black_hole_flutter/black_hole_flutter.dart';

import '/constants/constants.dart';

/// method to show a snackbar error
void showSnackbarError(
  BuildContext context, {
  String text = 'something went wrong...',
}) {
  context.scaffoldMessenger.showSnackBar(
    SnackBar(
      backgroundColor: AppColors.errorColor,
      content: Text(text),
    ),
  );
}
