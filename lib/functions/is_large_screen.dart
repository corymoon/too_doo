import 'package:flutter/material.dart';

import '/enums/enums.dart';
import 'responsive_size.dart';

bool isLargeScreen(BuildContext context) {
  var _width = ResponsiveSize.screenSize(context);
  var _isLargeScreen = _width == ScreenSize.medium ||
      _width == ScreenSize.large ||
      _width == ScreenSize.extraLarge ||
      _width == ScreenSize.extraExtraLarge;
  return _isLargeScreen;
}
