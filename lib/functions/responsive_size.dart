import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';

import '/enums/enums.dart';

class ResponsiveSize {
  static ScreenSize screenSize(BuildContext context) {
    double _width = context.mediaQuery.size.width;
    if (_width >= 1400) {
      return ScreenSize.extraExtraLarge;
    }
    if (_width >= 1200 && _width <= 1399) {
      return ScreenSize.extraLarge;
    }
    if (_width >= 992 && _width <= 1199) {
      return ScreenSize.large;
    }
    if (_width >= 768 && _width <= 991) {
      return ScreenSize.medium;
    }
    if (_width >= 576 && _width <= 767) {
      return ScreenSize.small;
    }
    return ScreenSize.extraSmall;
  }
}
