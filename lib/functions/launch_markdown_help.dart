import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

launchMarkdownHelp(BuildContext context) async {
  const url = 'https://www.markdownguide.org/basic-syntax/';
  var urlLaunchable = await canLaunch(url);
  if (urlLaunchable) {
    await launch(url);
  } else {
    showDialog(
      context: context,
      builder: (context) => const SimpleDialog(
        children: [Text('can\'t open URL')],
      ),
    );
  }
}
