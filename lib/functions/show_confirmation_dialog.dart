import 'package:flutter/material.dart';
import 'package:black_hole_flutter/black_hole_flutter.dart';

Future<bool> showConfirmationDialog(
  BuildContext context, {
  String message = 'are you sure you want to proceed?',
  String title = 'are you sure?',
}) async {
  bool _shouldProceed = false;

  await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        TextButton(
          onPressed: () {
            _shouldProceed = false;
            context.navigator.pop();
          },
          child: const Text('no'),
        ),
        TextButton(
          onPressed: () {
            _shouldProceed = true;
            context.navigator.pop();
          },
          child: const Text('yes'),
        ),
      ],
    ),
  );

  return _shouldProceed;
}
