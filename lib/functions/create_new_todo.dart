import 'package:black_hole_flutter/black_hole_flutter.dart';
import 'package:flutter/material.dart';

import '/services/services.dart';

Future<void> createNewTodo(
  BuildContext context,
  DatabaseService database,
  TextEditingController controller,
) async {
  await database.createNewTodo(controller.text.trim());
  context.navigator.pop();
  controller.clear();
}
