const functions = require("firebase-functions");
const admin = require("firebase-admin");
// import { firestore } from "firebase-admin";

admin.initializeApp();
const db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const createProfile = (userRecord, context) => {
  const { email, uid, displayName } = userRecord;
  return db
    .collection("users")
    .doc(uid)
    .set({
      email: email,
      uid: uid,
      displayName:
        displayName == undefined || displayName == "" || displayName == null
          ? "unnamed user"
          : displayName,
      photoURL: "",
      activeTodos: 0,
      archivedTodos: 0,
      completeTodos: 0,
      snoozedTodos: 0,
      lastActivity: admin.firestore.Timestamp.now(),
    })
    .catch(console.error);
};

exports.createUserDocument = functions.auth.user().onCreate(createProfile);

exports.changedTodoOnFeed = functions.firestore
  .document("users/{userId}/todos/{todoId}")
  .onUpdate((snapshot, context) => {
    const userId = context.params.userId;

    const before = snapshot.before.data();
    const after = snapshot.after.data();

    const oldState = before["state"];
    const newState = after["state"];

    let feedItemType = "";

    if (newState == "complete") {
      feedItemType = "completed";
    } else if (oldState == "complete" && newState == "active") {
      feedItemType = "incomplete";
    } else if (newState == "snoozed") {
      feedItemType = "snoozed";
    } else if (oldState == "snoozed" && newState == "active") {
      feedItemType = "wake";
    } else if (newState == "archived") {
      feedItemType = "archived";
    } else if (oldState == "archived") {
      feedItemType = "unarchive";
    } else if (newState == "priority") {
      feedItemType = "priority";
    } else if (oldState == "priority") {
      feedItemType = "unpriority";
    } else {
      return;
    }

    console.log(`userId => ${userId}`);
    console.log(`feedItemType => ${feedItemType}`);

    db.doc(`users/${userId}`)
      .get()
      .then((snap) => {
        const data = snap.data();
        const authorName = data["displayName"];
        const authorPhotoURL = data["photoURL"];
        const date = admin.firestore.Timestamp.now();

        db.collection("feed")
          .add({
            authorName: authorName,
            authorPhotoURL: authorPhotoURL,
            authorId: userId,
            feedItemType: feedItemType,
            date: date,
          })
          .catch(function (error) {
            console.log("error", error);
          });
      })
      .catch(function (error) {
        console.log("error:", error);
      });
  });

exports.newTodoOnFeed = functions.firestore
  .document("users/{userId}/todos/{todoId}")
  .onCreate((snapshot, context) => {
    const userId = context.params.userId;

    db.doc(`users/${userId}`)
      .get()
      .then((snap) => {
        const data = snap.data();
        const authorName = data["displayName"];
        const authorPhotoURL = data["photoURL"];
        const feedItemType = "created";
        const date = admin.firestore.Timestamp.now();

        db.collection("feed")
          .add({
            authorName: authorName,
            authorPhotoURL: authorPhotoURL,
            authorId: userId,
            feedItemType: feedItemType,
            date: date,
          })
          .catch(function (error) {
            console.log("error", error);
          });
      })
      .catch(function (error) {
        console.log("error:", error);
      });
  });

const everyMonthOnTheFirst = "0 0 1 * *";
exports.monthlyDeleteAllArchived = functions.pubsub
  .schedule(everyMonthOnTheFirst)
  .timeZone("America/Chicago")
  .onRun((context) => {
    db.collection("users").get().then(snapshot => {
      snapshot.forEach(document => {
        var userId = document.id;
        db.collection(`users/${userId}/todos`).get().then(snap => {
          snap.forEach(doc => {
            var data = doc.data();
            var state = data['state'];
            var saved = data['saved'];

            if (state == "archived" && saved != true) {
              db.collection(`users/${userId}/todos`).doc(doc.id).delete();
              return null;
            }
          });
        });
      });
    });
    // return null;
  });
